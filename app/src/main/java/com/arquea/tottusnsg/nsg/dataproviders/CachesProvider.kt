package com.arquea.tottusnsg.nsg.dataproviders


import com.arquea.tottusnsg.nsg.dataproviders.adapters.BigDecimalAdapter
import com.arquea.tottusnsg.nsg.service.models.Jerarquia
import com.arquea.tottusnsg.nsg.service.models.Producto
import com.arquea.tottusnsg.nsg.service.models.Tienda
import com.squareup.moshi.Moshi
import java.util.*
import kotlin.collections.ArrayList


class CachesProvider {
  companion object {
    const val TIENDAS_CACHE = "TiendasCache"
    const val SESION_JERARQUIAS = "JerarquiasCache"
    const val PREFIJO_PRODSJERARQUIAS = "ProductosJerarquia_"
    const val PRODUCTOS_VERIFICACION = "ProductosParaVerificacion"

    private fun obtenerMoshi(): Moshi? {
      return Moshi.Builder()
              .add(BigDecimalAdapter())
              .add(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory())
              .add(Date::class.java, com.squareup.moshi.adapters.Rfc3339DateJsonAdapter().nullSafe())
              .build()
    }

    fun finalizar() {
      LocalDataProvider.finalizarDoc(TIENDAS_CACHE)
    }

    var Tiendas: Array<Tienda>?
      get() {
        val doc = LocalDataProvider.obtenerDocumento(TIENDAS_CACHE)
        try {
          if (doc.getDate("expira") < Date()) {
            return null
          }
          val jsonAdapter = obtenerMoshi()!!.adapter<Array<Tienda>>(Array<Tienda>::class.java)
          return jsonAdapter.fromJson(doc.getString("data"))
        } catch (err: Exception) {
          return null
        }
      }
      set(value) {
        val doc = LocalDataProvider.obtenerDocumento(TIENDAS_CACHE)
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.DAY_OF_MONTH, 7)
        doc.setDate("expira", cal.time)
        val jsonAdapter = obtenerMoshi()!!.adapter<Array<Tienda>>(Array<Tienda>::class.java)
        doc.setString("data", jsonAdapter.toJson(value))
        LocalDataProvider.guardarDocumento(doc)
      }

    var ProductosParaVerificacion: Array<Producto>
      get() {
        val doc = LocalDataProvider.obtenerDocumento(PRODUCTOS_VERIFICACION)
        try {
          if (doc.getDate("expira") < Date()) {
            return emptyArray()
          }
          val jsonAdapter = obtenerMoshi()!!.adapter<Array<Producto>>(Array<Producto>::class.java)
          return jsonAdapter.fromJson(doc.getString("data"))!!
        } catch (err: Exception) {
          return emptyArray()
        }
      }
      set(value) {
        val doc = LocalDataProvider.obtenerDocumento(PRODUCTOS_VERIFICACION)
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.HOUR_OF_DAY, 4)
        doc.setDate("expira", cal.time)
        val jsonAdapter = obtenerMoshi()!!.adapter<Array<Producto>>(Array<Producto>::class.java)
        doc.setString("data", jsonAdapter.toJson(value))
        LocalDataProvider.guardarDocumento(doc)
      }

    var JerarquiasAsignadas: Array<Jerarquia>
      get() {
        val doc = LocalDataProvider.obtenerDocumento(SESION_JERARQUIAS)
        try {
          if (doc.getDate("expira") < Date()) {
            return emptyArray()
          }
          val jsonAdapter = obtenerMoshi()!!.adapter<Array<Jerarquia>>(Array<Jerarquia>::class.java)
          return jsonAdapter.fromJson(doc.getString("data")) ?: emptyArray()
        } catch (err: Exception) {
          return emptyArray()
        }
      }
      set(value) {
        val doc = LocalDataProvider.obtenerDocumento(SESION_JERARQUIAS)
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.HOUR_OF_DAY, 4)
        doc.setDate("expira", cal.time)
        val jsonAdapter = obtenerMoshi()!!.adapter<Array<Jerarquia>>(Array<Jerarquia>::class.java)
        doc.setString("data", jsonAdapter.toJson(value))
        LocalDataProvider.guardarDocumento(doc)
      }

    fun obtenerProductosEscaneados(codJerarquia:String):Array<Producto> {
      val doc = LocalDataProvider.obtenerDocumento(PREFIJO_PRODSJERARQUIAS + codJerarquia)
      try {
        if (doc.getDate("expira") < Date()) {
          return emptyArray()
        }
        val jsonAdapter = obtenerMoshi()!!.adapter<Array<Producto>>(Array<Producto>::class.java)
        return jsonAdapter.fromJson(doc.getString("data")) ?: emptyArray()
      } catch (err: Exception) {
        return emptyArray()
      }

    }
    fun registrarProductosEscaneados(codJerarquia: String, items: Array<Producto>) {
      val doc = LocalDataProvider.obtenerDocumento(PREFIJO_PRODSJERARQUIAS + codJerarquia)
      val cal = Calendar.getInstance()
      cal.time = Date()
      cal.add(Calendar.HOUR_OF_DAY, 4)
      doc.setDate("expira", cal.time)
      val jsonAdapter = obtenerMoshi()!!.adapter<Array<Producto>>(Array<Producto>::class.java)
      doc.setString("data", jsonAdapter.toJson(items))
      LocalDataProvider.guardarDocumento(doc)
    }

  }
}