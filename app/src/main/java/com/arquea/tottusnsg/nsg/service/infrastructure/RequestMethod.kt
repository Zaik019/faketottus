package com.arquea.tottusnsg.nsg.service.infrastructure

/**
 * Provides enumerated HTTP verbs
 */
enum class RequestMethod {
    GET, DELETE, HEAD, OPTIONS, PATCH, POST, PUT
}