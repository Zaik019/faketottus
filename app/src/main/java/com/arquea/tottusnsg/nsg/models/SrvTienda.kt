package com.arquea.tottusnsg.nsg.models

import com.arquea.tottusnsg.nsg.service.models.Tienda


data class SrvTienda(public val tiendaDeServicio: Tienda) {

  override fun toString(): String {
    return if (tiendaDeServicio.Descripcion != null) tiendaDeServicio.Descripcion.toString() else ""
  }
}