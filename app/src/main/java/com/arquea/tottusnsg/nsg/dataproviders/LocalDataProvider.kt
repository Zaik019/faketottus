package com.arquea.tottusnsg.nsg.dataproviders

import android.content.Context
import android.util.Log
import com.couchbase.lite.Database
import com.couchbase.lite.DatabaseConfiguration
import com.couchbase.lite.MutableDocument

class LocalDataProvider {
  companion object {
    private var DB: Database? = null
    private val IdGeneral: String = "General"
    fun inicializar(context: Context) {
      var config = DatabaseConfiguration(context)
      DB = Database("tottusnsg", config)
    }

    fun guardarDocumento(doc: MutableDocument) {
      DB!!.save(doc)
    }

    fun obtenerDocumento(idDoc: String): MutableDocument {
      try {
        var doc: MutableDocument? = DB!!.getDocument(idDoc).toMutable() ?: throw Exception();
        DB!!.save(doc)
        return doc!!
      } catch (err: Exception) {
        val mutableDoc = MutableDocument(idDoc)
        DB!!.save(mutableDoc);
        return mutableDoc
      }
    }

    fun finalizarDoc(idDoc: String) {
      try {
        var doc = DB!!.getDocument(idDoc);
        DB!!.delete(doc)
      } catch (e: Exception) {
        Log.w("TOTTUSNSG", "Error al limpiar local ${idDoc} data", e)
      }
    }

    fun finalizarLocalDoc() {
      finalizarDoc(IdGeneral)
    }

    fun obtener(llave: String): String? {
      try {
        var doc = DB!!.getDocument(IdGeneral).toMutable();
        if (doc == null) {
          return null
        }
        return doc.getString(llave);
      } catch (err: Exception) {
        return null
      }
    }

    fun actualizar(llave: String, valor: String?) {
      try {
        var doc = DB!!.getDocument(IdGeneral).toMutable();
        if (doc == null) {
          return
        }
        doc.setString(llave, valor)
        DB!!.save(doc)
      } catch (err: Exception) {
        val mutableDoc = MutableDocument(IdGeneral)
        DB!!.save(mutableDoc);
        actualizar(llave, valor);
      }
    }


  }

}