package com.arquea.tottusnsg.nsg.activities

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.nsg.adapters.InfoMapaAdapter
import com.arquea.tottusnsg.nsg.dataproviders.CachesProvider
import com.arquea.tottusnsg.nsg.dataproviders.SesionProvider
import com.arquea.tottusnsg.nsg.models.SrvPais
import com.arquea.tottusnsg.nsg.models.SrvTienda
import com.arquea.tottusnsg.nsg.service.apis.TiendasApi
import com.arquea.tottusnsg.nsg.service.models.Tienda
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_elegirtienda.*
import pe.com.falabella.faketottus.R
import kotlin.system.exitProcess

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ElegirTiendaActivity : BaseActivity(), OnMapReadyCallback {

  private var dataTiendas: Map<String?, List<SrvTienda>>? = null
  private var instanciaGMap: GoogleMap? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_elegirtienda)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    mapa.onCreate(savedInstanceState)
    mapa.getMapAsync(this)
    //
    pedirPermisos()
    cargarPaisesYTiendas {
      if (it && !isDestroyed) {
        inicializarDisplay()
        inicializarEventos()
        cargarTiendasEnMapa()
      } else if (!it && !isDestroyed) {
        manejarErrorDeConexion()
      }
    }
  }

  private fun mostrarPaisesYTiendas(tiendasRaw: Array<Tienda>, resultadoFn: (Boolean) -> Unit) {
    dataTiendas = tiendasRaw.map { x -> SrvTienda(x) }.groupBy { p -> p.tiendaDeServicio.CodigoPais }
    val paises = dataTiendas!!.keys.map { p -> SrvPais(p) }
    val tiendas = dataTiendas!![paises.first().codPais]
    val tiendasAdapter = ArrayAdapter<SrvTienda>(this, R.layout.spinner_item, tiendas)
    val paisAdapter = ArrayAdapter<SrvPais>(this, R.layout.spinner_item_alterno, paises)
    this.runOnUiThread {
      paisAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
      tiendasAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
      spinner_pais.adapter = paisAdapter
      spinner_tienda.adapter = tiendasAdapter
    }
    resultadoFn(true)
  }

  private fun cargarPaisesYTiendas(resultadoFn: (Boolean) -> Unit) {
    try {
      val cache = CachesProvider.Tiendas
      if (cache != null) {
        mostrarPaisesYTiendas(cache, resultadoFn)
      } else {
        runApiCall({
          val tiendas = TiendasApi(it).listarTiendas()
          CachesProvider.Tiendas = tiendas
          mostrarPaisesYTiendas(tiendas, resultadoFn)
        }, {
          resultadoFn(false)
        })
      }
    } catch (err: Exception) {
      Log.e("TEMPORAL", "?????????????????????????????", err)
    }

  }

  private fun manejarErrorDeConexion() {
    Log.e("TOTTUSNSG", "No es posible conectar al servicio, saliendo...")
    AlertDialog.Builder(this)
            .setTitle("Error al conectar")
            .setMessage("No se ha podido conectar al servicio, por favor intente después")
            .setPositiveButton(android.R.string.ok, null)
            .show()
            .setOnDismissListener {
              finish()
              exitProcess(1)
            }
  }

  private fun inicializarDisplay() {
    Handler().postDelayed({
      runOnUiThread {
        modalSelectorTienda.visibility = View.VISIBLE
        modalSelectorTienda.alpha = 0.0f
        modalSelectorTienda.animate().alpha(1f).setDuration(400).setListener(null).start()
      }
    }, 500)
  }

  private fun inicializarEventos() {
    btnTiendaElegida.setOnClickListener {
      try {
        val tiendaSeleccionada = (spinner_tienda.selectedItem as SrvTienda).tiendaDeServicio
        SesionProvider.TiendaSeleccionada = tiendaSeleccionada.CodigoTienda
        SesionProvider.SesionCodigo = tiendaSeleccionada.Sesion
        val intent = Intent(applicationContext, InicioActivity::class.java)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
      } catch (inputError: Exception) {
        AlertDialog.Builder(this).setTitle("No es posible continuar")
                .setMessage("Debe seleccionar una tienda para continuar").setPositiveButton(android.R.string.ok, null).show()
      }
    }

    val activity = this
    spinner_pais.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onNothingSelected(parent: AdapterView<*>?) {}
      override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val tiendasAdapter = ArrayAdapter<SrvTienda>(activity, R.layout.spinner_item, dataTiendas!![(spinner_pais.selectedItem as SrvPais).codPais])
        tiendasAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        spinner_tienda.adapter = tiendasAdapter
      }
    }
    spinner_tienda.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onNothingSelected(parent: AdapterView<*>?) {}
      override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        centrarMapaEnTienda(spinner_tienda.selectedItem as SrvTienda)
      }
    }
  }


  @TargetApi(Build.VERSION_CODES.M)
  private fun pedirPermisos(): Boolean {
    val permisos = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    val fineLoc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
    val coarseLoc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
    return if (fineLoc != PackageManager.PERMISSION_GRANTED || coarseLoc != PackageManager.PERMISSION_GRANTED) {
      if (ActivityCompat.shouldShowRequestPermissionRationale(this as Activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
        AlertDialog.Builder(this)
                .setTitle("Permisos")
                .setMessage("La aplicación necesita su ubicación para sugerirle la tienda.")
                .setPositiveButton(android.R.string.ok, null)
                .show()
        ActivityCompat.requestPermissions(this as Activity, permisos, 1001)
      } else {
        ActivityCompat.requestPermissions(this as Activity, permisos, 1001)
      }
      false
    } else
      true
  }


  override fun onResume() {
    super.onResume()
    mapa.onResume()
  }

  override fun onStart() {
    super.onStart()
    mapa.onStart()
  }

  override fun onStop() {
    super.onStop()
    mapa.onStop()
  }

  override fun onPause() {
    mapa.onPause()
    super.onPause()
  }

  override fun onDestroy() {
    mapa.onDestroy()
    super.onDestroy()
  }

  override fun onLowMemory() {
    super.onLowMemory()
    mapa.onLowMemory()
  }

  override fun onMapReady(googleMap: GoogleMap) {
    val mapaAdapter = InfoMapaAdapter(this)
    googleMap.setInfoWindowAdapter(mapaAdapter)
    mapa.onResume()
    instanciaGMap = googleMap
    cargarTiendasEnMapa()
  }

  @SuppressLint("MissingPermission")
  private fun cargarTiendasEnMapa() {
    if (instanciaGMap == null || dataTiendas == null) {
      return
    }
    var curLat: Double? = null
    var curLng: Double? = null
    val icon = BitmapDescriptorFactory.fromResource(R.drawable.punto_mapa)

    Handler().postDelayed({
      for (entrada in dataTiendas!!) {
        for (tienda in entrada.value) {
          if (tienda.tiendaDeServicio.Latitud != null && tienda.tiendaDeServicio.Longitud != null) {
            if (curLat == null || curLng == null) {
              curLat = tienda.tiendaDeServicio.Latitud.toDouble()
              curLng = tienda.tiendaDeServicio.Longitud.toDouble()
            }
            runOnUiThread {
              if (!this.isDestroyed) {
                val infoMarker = instanciaGMap!!.addMarker(MarkerOptions()
                        .position(LatLng(tienda.tiendaDeServicio.Latitud.toDouble(), tienda.tiendaDeServicio.Longitud.toDouble()))
                        .title(tienda.tiendaDeServicio.Descripcion)
                        .icon(icon))
                infoMarker.showInfoWindow()
              }
            }
            Thread.sleep(50)
          }
        }
      }
    }, 1500)


    Handler().postDelayed({
      try {
        val locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val loc = locationManager.getLastKnownLocation(locationManager.getBestProvider(Criteria(), false))
        val tienda = dataTiendas!!.values.flatten().map { p -> p.tiendaDeServicio }.filter { p -> p.Latitud != null && p.Longitud != null }.map { x ->
          val xx = Math.pow(x.Latitud!!.toDouble() - loc.latitude, 2.toDouble())
          val yy = Math.pow(x.Longitud!!.toDouble() - loc.longitude, 2.toDouble())
          Pair(Math.sqrt(Math.sqrt(xx + yy)), x)
        }.sortedBy { r -> r.first }.firstOrNull()?.second
        if (tienda != null) {
          runOnUiThread {
            instanciaGMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(tienda.Latitud!!.toDouble(), tienda.Longitud!!.toDouble()), 15f))
          }
        }
      } catch (error: Exception) {
      }
    }, 300)

  }

  private fun centrarMapaEnTienda(srvTienda: SrvTienda?) {
    if (instanciaGMap == null || dataTiendas == null || srvTienda == null) {
      return
    }
    runOnUiThread {
      val tienda = srvTienda.tiendaDeServicio
      if (tienda.Latitud != null && tienda.Longitud != null) {
        instanciaGMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(tienda.Latitud.toDouble(), tienda.Longitud.toDouble()), 15f))
      }
    }
  }
}
