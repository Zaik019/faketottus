package com.arquea.tottusnsg.nsg.models

data class SrvPais(public val codPais: String?) {

  override fun toString(): String {
    when(codPais?.toUpperCase()) {
      "PER" -> return "Perú"
      "CHL" -> return "Chile"
    }
    return "Desconocido"
  }
}