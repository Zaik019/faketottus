package com.arquea.tottusnsg.nsg.dataproviders

class SesionProvider {
  companion object {
    fun finalizar() {
      LocalDataProvider.finalizarLocalDoc()
    }

    var SesionCodigo: String?
      get() {
        return LocalDataProvider.obtener("SesionCodigo")
      }
      set(value) {
        LocalDataProvider.actualizar("SesionCodigo", value)

      }
    var IdUsuario: String?
      get() {
        return LocalDataProvider.obtener("SesionIdUsuario")
      }
      set(value) {
        LocalDataProvider.actualizar("SesionIdUsuario", value)
      }
    var NombreUsuario: String?
      get() {
        return LocalDataProvider.obtener("SesionNombreUsuario")
      }
      set(value) {
        LocalDataProvider.actualizar("SesionNombreUsuario", value)
      }
    var Correo: String?
      get() {
        return LocalDataProvider.obtener("SesionCorreo")
      }
      set(value) {
        LocalDataProvider.actualizar("SesionCorreo", value)
      }

    var TiendaSeleccionada: String?
      get() {
        return LocalDataProvider.obtener("SesionTienda")
      }
      set(value) {
        LocalDataProvider.actualizar("SesionTienda", value)
      }
  }
}