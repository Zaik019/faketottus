package com.arquea.tottusnsg.nsg.service.infrastructure

import com.arquea.tottusnsg.nsg.dataproviders.adapters.BigDecimalAdapter
import com.squareup.moshi.Moshi
import java.util.*


object Serializer {
  @JvmStatic
  val moshi: Moshi = Moshi.Builder()
          .add(BigDecimalAdapter())
          .add(com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory())
          .add(Date::class.java, com.squareup.moshi.adapters.Rfc3339DateJsonAdapter().nullSafe())
          .build()
}
