package com.arquea.tottusnsg.nsg.dataproviders.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal


class BigDecimalAdapter {
  @ToJson
  fun toJson(num: BigDecimal): Float {
    return num.toFloat()
  }

  @FromJson
  fun fromJson(num: Float): BigDecimal {
    return num.toBigDecimal()
  }
}