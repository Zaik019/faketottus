package com.arquea.tottusnsg.nsg.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.nsg.dataproviders.SesionProvider
import com.arquea.tottusnsg.nsg.service.apis.TiendasApi
import com.microsoft.aad.adal.*
import kotlinx.android.synthetic.main.activity_login.*
import pe.com.falabella.faketottus.BuildConfig
import pe.com.falabella.faketottus.R

class LoginActivity : BaseActivity() {

  private lateinit var mContext: AuthenticationContext
  private var mResult: AuthenticationResult? = null
  private var iniciado = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)
    btnIngresarApp.setOnClickListener {
      procesarLogin()
    }
  }

  override fun onResume() {
    super.onResume()
    var autoiniciar = true
    if (intent.extras != null && intent.extras.containsKey("autoiniciar")) {
      autoiniciar = intent.extras.get("autoiniciar") as Boolean
    }
    if (!autoiniciar) {
      iniciado = true
    }

    if (!iniciado) {
      iniciado = true
      procesarLogin()
    } else {
      habilitarBotonLogin()
    }
  }

  private fun habilitarBotonLogin() {
    try {
      runOnUiThread {
        cargador.visibility = View.GONE
        btnIngresarApp.visibility = View.VISIBLE
      }
    } catch (e: Exception) {
      Log.e("TOTTUSNSG", "Error al intentar mostrar cargador", e)
    }
  }

  private fun deshabilitarBotonLogin() {
    try {
      runOnUiThread {
        cargador.visibility = View.VISIBLE
        btnIngresarApp.visibility = View.GONE
      }
    } catch (e: Exception) {
      Log.e("TOTTUSNSG", "Error al intentar cambiar estado de botones", e)
    }
  }

  private fun procesarLogin(retry: Boolean = true) {
    mContext = AuthenticationContext(this, BuildConfig.ADNATIVE_DIRECTORIO, true)
    val self = this
    val callback = object : AuthenticationCallback<AuthenticationResult> {
      override fun onError(exc: Exception?) {
        Log.w(getString(R.string.NombreAppLog), "Error al iniciar sesión", exc)
        if (exc is AuthenticationException) {
          if (retry) {
            Handler().postDelayed({
              if (!isDestroyed) {
                procesarLogin(false)
              } else {

              }
            }, 2000)
            return
          } else {
            AlertDialog.Builder(self)
                    .setTitle("Inicio de sesión")
                    .setMessage("Debe iniciar con un usuario y contraseña válidos y con permisos para acceder a la aplicación.")
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
            try {
              mContext.cache.removeAll()
              var p = applicationContext.getSharedPreferences("ActiveDirectoryAuthenticationLibrary", Context.MODE_PRIVATE)
              var editor = p.edit()
              editor.remove("cache")
              editor.commit()
            } catch (e: Exception) {
              Log.e("TOTTUSNSG", "Error al intentar borrar la sesión del usuario", e)
            }
          }
        } else {
          AlertDialog.Builder(self)
                  .setTitle("Inicio de sesión")
                  .setMessage("Usuario o contraseña incorrectos o su cuenta no tiene permisos para acceder a esta aplicación.")
                  .setPositiveButton(android.R.string.ok, null)

        }
        habilitarBotonLogin()
      }

      override fun onSuccess(result: AuthenticationResult?) {
        mResult = result
        if (result?.getAccessToken() == null || result.getAccessToken().isEmpty()) {
          Log.d("ADAL", "Token is empty")
          AlertDialog.Builder(self)
                  .setTitle("Inicio de sesión")
                  .setMessage("Debe iniciar con un usuario y contraseña válidos y con permisos para acceder a la aplicación.")
                  .setPositiveButton(android.R.string.ok, null)
                  .show()
          habilitarBotonLogin()
        } else {
          Log.d("ADAL", "Sesión conseguida, token aparentemente válido, continuamos...")
          SesionProvider.IdUsuario = result.userInfo.userId
          SesionProvider.NombreUsuario = result.userInfo.givenName.split(" ").firstOrNull() + " " + result.userInfo.familyName.split(" ").firstOrNull()
          SesionProvider.Correo = result.userInfo.displayableId
          continuarConSeleccionTienda()
        }
      }
    }
    deshabilitarBotonLogin()
    mContext.acquireToken(this, BuildConfig.ADNATIVE_RECURSO, BuildConfig.ADNATIVE_APP_ID, BuildConfig.ADNATIVE_APP_REDIRECTURL, PromptBehavior.Auto, callback)
  }

  private fun continuarConSeleccionTienda() {
    if (SesionProvider.TiendaSeleccionada != null) { //Hay ya una tienda elegida, la usamos
      cargarHome()
      runApiCall({
        val tienda = TiendasApi(it).listarTiendas().filter { x -> x.CodigoTienda == SesionProvider.TiendaSeleccionada }.first()
        SesionProvider.SesionCodigo = tienda.Sesion //Actualizamos la sesión
        Log.i("TOTTUSNSG", "Sesión actualizada, nuevo sesión código generado")
      }, {
        cargarSelectorTiendas()
      })
    } else {
      cargarSelectorTiendas()
    }
  }

  private fun cargarHome() {
    val intent = Intent(applicationContext, InicioActivity::class.java)
    startActivity(intent)
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    finish()
  }

  private fun cargarSelectorTiendas() {
    val intent = Intent(applicationContext, ElegirTiendaActivity::class.java)
    startActivity(intent)
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    finish()
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
      super.onActivityResult(requestCode, resultCode, data)
      mContext.onActivityResult(requestCode, resultCode, data)
  }
}
