/**
* BFF MOVIL - TOTTUS NSG
* BFF Móvil para el Aplicativo de Nivel de Servicio en Góndola y Reposición - TOTTUS PERU.
*
* OpenAPI spec version: 1.0.0
* Contact: sistemas@arquea.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/
package com.arquea.tottusnsg.nsg.service.models


/**
 * 
 * @param Estado 
 */
data class Healthcheck (
    val Estado: kotlin.String? = null
) {

}

