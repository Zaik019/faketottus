package com.arquea.tottusnsg.nsg.adapters

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import pe.com.falabella.faketottus.R

class InfoMapaAdapter(var context: Context) : GoogleMap.InfoWindowAdapter {

  public override fun getInfoWindow(marker:Marker):View? {
    val vista = (context as Activity).layoutInflater.inflate(R.layout.custom_infomapa, null)
    val txtTienda = vista.findViewById<TextView>(R.id.nombre_tienda)
    txtTienda.text = marker.title
    return  vista
  }

  public override fun getInfoContents(marker:Marker): View?
  {
    return  null
  }
}
