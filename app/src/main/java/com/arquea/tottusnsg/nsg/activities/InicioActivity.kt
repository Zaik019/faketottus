package com.arquea.tottusnsg.nsg.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.nsg.dataproviders.CachesProvider
import com.arquea.tottusnsg.nsg.dataproviders.SesionProvider
import com.arquea.tottusnsg.nsg.models.SrvTienda
import com.arquea.tottusnsg.nsg.service.apis.TiendasApi
import com.microsoft.aad.adal.AuthenticationContext
import kotlinx.android.synthetic.main.activity_inicio.*
import kotlinx.android.synthetic.main.app_bar_inicio.*
import pe.com.falabella.faketottus.BuildConfig
import pe.com.falabella.faketottus.R


class InicioActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

  private var menuActual: Menu? = null

  private fun obtenerFragmentoVisible(): Fragment? {
    val fragmentManager = supportFragmentManager
    val fragments = fragmentManager.fragments
    for (fragment in fragments) {
      if (fragment != null && fragment.isVisible)
        return fragment
    }
    return null
  }

  fun irAFragmento(fragmento: Fragment, agregarAlStack: Boolean = true, animFade: Boolean = false) {
    val ultimoFragmento = obtenerFragmentoVisible()
    if (ultimoFragmento != null) if (fragmento::class == ultimoFragmento::class) {
      drawer_layout.closeDrawer(GravityCompat.START)
      return //Ya está en pantalla
    }
    val transaction = supportFragmentManager.beginTransaction()
    if (animFade) {
      transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
    } else {
      transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
    }
    transaction.replace(R.id.contenedor, fragmento)
    if (agregarAlStack) {
      transaction.addToBackStack(null)
    }
    transaction.commit()
    drawer_layout.closeDrawer(GravityCompat.START)

  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_inicio)
    setSupportActionBar(toolbar)
    //
    fab.setOnClickListener { view ->
      Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
              .setAction("Action", null).show()
    }
    val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
    drawer_layout.addDrawerListener(toggle)
    toggle.syncState()

    nav_view.setNavigationItemSelectedListener(this)
    //Opciones adicionales
    btnDetalleProducto.setOnClickListener {
      //irAFragmento(DetalleProductoScannerFragment())
    }
    btnSesionesActivas.setOnClickListener {
      //irAFragmento(SesionesActivasFragment())
    }
    btnSesionesActivasPrincipal.setOnClickListener {
      //irAFragmento(SesionesActivasFragment())
    }
    btnJerarquias.setOnClickListener {
      //irAFragmento(Jerarquias())
    }
    btnJerarquiasEscaneadas.setOnClickListener {
      //irAFragmento(JerarquiasEscaneadasFragment())
    }
    btnCaminataQuiebre.setOnClickListener {
      //irAFragmento(CaminataQuiebreScannerFragment())
    }
    btnVerificarPrecios.setOnClickListener {
      //irAFragmento(VerificacionPrecioFragment())
    }
    btnReposicionXpress.setOnClickListener {
      //irAFragmento(ReposicionXpressFragment())
    }

    if (CachesProvider.JerarquiasAsignadas.isNotEmpty()) {
      //irAFragmento(SesionesActivasFragment(), false)
    } else {
      //irAFragmento(Jerarquias(), false)
    }
  }

  override fun onBackPressed() {
    if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
      drawer_layout.closeDrawer(GravityCompat.START)
    } else {
      super.onBackPressed()
    }
  }

  private fun cargarSelectorTiendas() {
    val intent = Intent(applicationContext, ElegirTiendaActivity::class.java)
    startActivity(intent)
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    finish()
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    // Inflate the menu; this adds items to the action bar if it is present.
    menuInflater.inflate(R.menu.inicio, menu)
    //Selector de tienda
    txtNombreUsuario.text = SesionProvider.NombreUsuario
    txtCorreoUsuario.text = SesionProvider.Correo
    val tiendas = CachesProvider.Tiendas
    if (tiendas != null) {
      val tiendasAdapter = ArrayAdapter<SrvTienda>(this, R.layout.spinner_menuitem, tiendas.map { x -> SrvTienda(x) })
      tiendasAdapter.setDropDownViewResource(R.layout.spinner_menuitemdropdown)
      spinner_tiendamenu.adapter = tiendasAdapter
      spinner_tiendamenu.setSelection(tiendas.indexOfFirst { x -> x.CodigoTienda == SesionProvider.TiendaSeleccionada })
      spinner_tiendamenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
          SesionProvider.TiendaSeleccionada = (spinner_tiendamenu.selectedItem as SrvTienda).tiendaDeServicio.CodigoTienda
          SesionProvider.SesionCodigo = (spinner_tiendamenu.selectedItem as SrvTienda).tiendaDeServicio.Sesion
          runOnUiThread { drawer_layout.closeDrawer(GravityCompat.START) }
          runApiCall({
            val tienda = TiendasApi(it).listarTiendas().first { x -> x.CodigoTienda == SesionProvider.TiendaSeleccionada }
            SesionProvider.SesionCodigo = tienda.Sesion //Actualizamos la sesión
            Log.i("TOTTUSNSG", "Sesión actualizada, nuevo sesión código generado")
          }, {
            cargarSelectorTiendas()
          })
        }
      }
    }
    //Cierre de sesión
    btnCerrarSesion.setOnClickListener {
      SesionProvider.finalizar()
      CachesProvider.finalizar()
      val mContext = AuthenticationContext(this, BuildConfig.ADNATIVE_DIRECTORIO, true)
      mContext.cache.removeAll()

      val intent = Intent(applicationContext, LoginActivity::class.java)
      intent.putExtra("autoiniciar", false)
      startActivity(intent)
      finish()
    }
    menuActual = menu
    return super.onCreateOptionsMenu(menu)
    //return true
  }

  fun toggleMenu(id: Int, toggleMostrar: Boolean) {
    if (menuActual == null) {
      return
    }
    val menuItem: MenuItem = menuActual!!.findItem(id)
    menuItem.isVisible = toggleMostrar
  }

  lateinit var alPrecionarBotonEscaneo: (MenuItem) -> Boolean

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.itemmenu_busqueda -> {
        TransitionManager.beginDelayedTransition(toolbar)
        true
      }
      R.id.itemmenu_escanear -> {
        try {
          alPrecionarBotonEscaneo(item)
        } catch (err: Exception) {
        }
        true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }

  fun mostrarToolbar() {
    Handler().postDelayed({
      val toolbarLayoutInterno = toolbarLayout
      if (toolbarLayoutInterno != null) {
        (this as AppCompatActivity).supportActionBar!!.show()
        toolbarLayoutInterno.animate().translationY(0F).interpolator = AccelerateInterpolator(2.0F)
      }
    }, 150)
  }

  fun ocultarToolbar() {
    val toolbar = toolbarLayout
    if (toolbar != null) {
      toolbar.animate().translationY(-1 * toolbar.height.toFloat()).interpolator = AccelerateInterpolator(2.0F)
      Handler().postDelayed({
        runOnUiThread {
          try {
            (this as AppCompatActivity).supportActionBar!!.hide()
          } catch (e: Exception) {
          }
        }
      }, 800)
    }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    // Handle navigation view item clicks here.
    val fragment: Fragment? = null
    if (fragment != null) {
      val ft = supportFragmentManager.beginTransaction()
      ft.replace(R.id.contenedor, fragment)
      ft.commit()
    }

    drawer_layout.closeDrawer(GravityCompat.START)
    return true
  }

  lateinit var alRecibirPermisos: (Int, Array<out String>, IntArray) -> Boolean
  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    try {
      alRecibirPermisos(requestCode, permissions, grantResults)
    } catch (err: Exception) {
      Log.w("TOTTUSNSG", "No estaba especificado el evento AlRecibirPermiso", err)
    }
  }
}
