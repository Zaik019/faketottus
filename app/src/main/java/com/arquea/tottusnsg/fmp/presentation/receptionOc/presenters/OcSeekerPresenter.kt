package com.arquea.tottusnsg.fmp.presentation.receptionOc.presenters

import android.content.Context
import com.arquea.tottusnsg.fmp.data.entities.OcEntity
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.presentation.receptionOc.contracts.OcSeekerContract

class OcSeekerPresenter(
        context : Context,
        private val ocSeekerView : OcSeekerContract.View
) : OcSeekerContract.Presenter{
    private val sessionManager : SessionManager

    init {
        ocSeekerView.presenter = this
        sessionManager = SessionManager(context)
    }

    override fun getOc(ocId: String) {
        var ocEntity : OcEntity? = null
        for (oc in OcEntity.mockOcList()){
            if (ocId == oc.ocId){
                ocEntity = oc
            }
        }
        if (ocEntity == null) ocSeekerView.setError("Número de OC inválido: $ocId")
        else ocSeekerView.showOc(ocEntity)
    }

    override fun start() {

    }
}