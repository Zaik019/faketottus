package com.arquea.tottusnsg.fmp.data.dto.responses

open class BaseResponse {
    var message : String? = null
    var state : Int?=null
}