package com.arquea.tottusnsg.fmp.data.dto.responses

import com.arquea.tottusnsg.fmp.data.entities.ReportEntity

class ReportResponse : BaseResponse() {
    var reportEntity : ReportEntity? = null
}