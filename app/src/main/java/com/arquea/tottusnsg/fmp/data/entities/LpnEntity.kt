package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class LpnEntity(
        var lpnKey: String,
        var lpnId: String,
        var lpnCode: String,
        var classification: Int,
        var priorization: Double,
        var isBroken: Boolean,
        var isHighValue: Boolean,
        var isPending: Boolean,
        var warehouseId: Int,
        var warehouseName: String,
        var skuList: List<SkuEntity>
) : Serializable {

    var idTim : String ? = null

    companion object {
        fun mockClassifiedLpnList(): List<LpnEntity> {
            val list: ArrayList<LpnEntity> = ArrayList()
            list.add(LpnEntity("1234k", "1234567890987", "1234567890987", 1,
                    71.0, true, false, true,-1 , "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("2345k", "2345678909870", "2345678909876", 2,
                    31.0, true, false, true,-1 , "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("4569k", "4569098765432", "4569890987654", 1,
                    80.0, false, false, false, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("1235k", "1235123456789", "1235567890987", 1,
                    65.0, false, false, true, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("2346k", "2346123456789", "2346678909876", 2,
                    39.0, false, false, true, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("4567k", "4567098765432", "4567890987654", 3,
                    0.0, false, false, false, 22, "Fila B1",
                    SkuEntity.skuListMock()))
            return list
        }

        fun mockNonClassifiedLpnList(): List<LpnEntity> {
            val list: ArrayList<LpnEntity> = ArrayList()
            list.add(LpnEntity("1234k", "1234567890987", "1234567890987", 0,
                    0.0, false, false, false,-1 , "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("2345k", "2345678909870", "2345678909876", 0,
                    0.0, false, false, false,-1 , "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("4569k", "4569098765432", "4569890987654", 0,
                    0.0, false, false, false, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("1235k", "1235123456789", "1235567890987", 0,
                    0.0, false, false, false, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("2346k", "2346123456789", "2346678909876", 0,
                    0.0, false, false, false, -1, "",
                    SkuEntity.skuListMock()))
            list.add(LpnEntity("4567k", "4567098765432", "4567890987654", 0,
                    0.0, false, false, false, -1, "",
                    SkuEntity.skuListMock()))
            return list
        }
        fun mockOcClassifiedLpnList(): List<LpnEntity> {
            val list: ArrayList<LpnEntity> = ArrayList()
            list.add(LpnEntity("1234k", "1234567890987", "1234567890987", 1,
                    71.0, true, false, true,-1 , "",
                    SkuEntity.skuOcListMock()))
            list.add(LpnEntity("2345k", "2345678909870", "2345678909876", 2,
                    31.0, true, false, true,-1 , "",
                    SkuEntity.skuOcListMock()))
            list.add(LpnEntity("4569k", "4569098765432", "4569890987654", 1,
                    80.0, false, false, false, -1, "",
                    SkuEntity.skuOcListMock()))
            return list
        }

        fun mockOcNonClassifiedLpnList(): List<LpnEntity> {
            val list: ArrayList<LpnEntity> = ArrayList()
            list.add(LpnEntity("1234k", "1234567890987", "1234567890987", 0,
                    0.0, false, false, false,-1 , "",
                    SkuEntity.skuOcListMock()))
            list.add(LpnEntity("2345k", "2345678909870", "2345678909876", 0,
                    0.0, false, false, false,-1 , "",
                    SkuEntity.skuOcListMock()))
            list.add(LpnEntity("4569k", "4569098765432", "4569890987654", 0,
                    0.0, false, false, false, -1, "",
                    SkuEntity.skuOcListMock()))
            return list
        }

    }
}