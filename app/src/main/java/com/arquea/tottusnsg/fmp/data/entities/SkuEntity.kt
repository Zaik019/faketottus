package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class SkuEntity(
        val lpnKey: String,
        val lpnId: String,
        val skuKey: String,
        val sequenceId: Int,
        val hierarchy:String,
        val name: String,
        val eanCode: String,
        val sentAmount: Int,
        val receivedAmount: Int,
        val replaceAmount: Int,
        val storeAmount: Int,
        val maxCapacity: Int,
        val onHand: Int,
        val isBroken: Boolean,
        val isHighValue: Boolean,
        val corridors: List<CorridorEntity>
) : Serializable {

    var currentCorridor :String = "-"
        get() {
            return if (corridors.isNotEmpty() && corridors[0].description.trim() != "") corridors[0].description
            else "-"
        }

    companion object {
        fun skuListMock(): List<SkuEntity> {
            val list: ArrayList<SkuEntity> = ArrayList()
            list.add(SkuEntity("1234K", "1234", "sku1", 1,"","Televisor Led LG 55' ", "1234767890987", 20,
                    20, 15, 5, 15, 0, true, true,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku2",2, "","Trident FRESA", "1634567890987", 14,
                    14, 14, 0, 60, 0, true, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku3", 3,"","Trident LIMON", "1230567890987", 63,
                    63, 50, 13, 50, 0, true, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku4", 4,"","Trident", "1234567890981", 222,
                    222, 22, 200, 200, 178, false, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku5",5, "","Ayudín a la grasa le pone fin", "1934567890987", 69,
                    69, 9, 60, 60, 51, false, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku6",6, "","Baigon", "7234567890987", 96,
                    96, 0, 96, 100, 100, false, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku7", 7,"","Leche materna Ensure", "1234567890987", 83,
                    83, 0, 83, 120, 120, false, true,
                    CorridorEntity.corridorListMock()))
            return list
        }
        fun skuOcListMock(): List<SkuEntity> {
            val list: ArrayList<SkuEntity> = ArrayList()
            list.add(SkuEntity("41674679", "2000416746798", "sku1", 1,"","PINA GOLDEN TOTTUS CHIQUI 150GR", "2000416746798", 5,
                    5, 5, 0, 5, 0, true, true,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("41674680", "2000416746804", "sku2",2, "","PINA GOLDEN TOTTUS TROZOS", "2000416746804", 5,
                    5, 5, 0, 10, 0, true, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("41674681", "2000416746811", "sku3", 3,"","TROZOS MELON TOTTUS 300GR", "2000416746811", 2,
                    2, 2, 0, 10, 0, true, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku4", 4,"","ENSALADA TOTTUS PINA SANDIA", "1234567890981", 222,
                    222, 22, 200, 200, 178, false, false,
                    CorridorEntity.corridorListMock()))
            list.add(SkuEntity("1234k", "1234", "sku5",5, "","FRUTA MELON TOTTUS 150GR", "1934567890987", 69,
                    69, 9, 60, 60, 51, false, false,
                    CorridorEntity.corridorListMock()))
            return list
        }

    }
}