package com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters

import android.content.Context
import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.TimResponse
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl.TimFakeRepository
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.TimDetailContract

class TimDetailPresenter(
        private val context: Context,
        private val view: TimDetailContract.View
) : TimDetailContract.Presenter {
    override fun setTim(tim: TimEntity) {
        timEntity = tim
        start()
    }

    private val timRepository: TimFakeRepository
    private lateinit var timEntity:TimEntity
    private val sessionManager : SessionManager

    init {
        view.presenter = this
        timRepository = TimFakeRepository.getInstance()
        sessionManager = SessionManager(context)
    }

    override fun start() {
        if (timEntity != null) {
            view.showTim(timEntity)
        }
    }

    override fun getLpn(lpn: LpnEntity) {
        view.showLpnDetail(lpn)
    }

    override fun checkTimArriveDate(timId: String) {
        val timRequest = TimRequest(timId,sessionManager.storeId)

        timRepository.getClassifiedTimById(timRequest, object : BaseRequestCallback.ApiResponse<TimResponse> {
            override fun onSuccess(response: TimResponse) {
                if (view.isActive) view.showTim(response.timEntity!!)
            }

            override fun onError(errorMessage: String) {
                view.setError(errorMessage)
            }

            override fun onNetworkError() {
                view.setError("No se pudo conectar al servidor")
            }
        })
    }
}