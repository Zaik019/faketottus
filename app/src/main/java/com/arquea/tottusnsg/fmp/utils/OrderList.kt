package com.arquea.tottusnsg.fmp.utils

import com.arquea.tottusnsg.fmp.data.entities.CorridorWithSkuEntity
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.SkuEntity

object OrderList {
    //LPN ordenado por quebrado y por indice de priorizacion
    fun orderLpn(list: ArrayList<LpnEntity>) : ArrayList<LpnEntity>{
        val tempList : ArrayList<LpnEntity> = ArrayList()
        val isBrokeList : ArrayList<LpnEntity> = ArrayList()
        val isNonBrokeList : ArrayList<LpnEntity> = ArrayList()
        for(lpn in list){
            if (lpn.isBroken){
                isBrokeList.add(lpn)
            }
            else{
                isNonBrokeList.add(lpn)
            }
        }
        val list1 = isBrokeList.sortedWith(compareBy { it.priorization }).asReversed()
        val list2 = isNonBrokeList.sortedWith(compareBy { it.priorization }).asReversed()
        tempList.addAll(list1)
        tempList.addAll(list2)
        return tempList
    }

    //SKU ordenado por quebrado,
    // QUEBRADO: cantidad de quebrado por pasillo, si hay empate -> mayor cantidad de unidades de sku
    //

    fun orderSku(list: ArrayList<SkuEntity>) : ArrayList<SkuEntity>{
        val corridorList : ArrayList<CorridorWithSkuEntity> = ArrayList()
        var tempList : ArrayList<SkuEntity> = ArrayList()
        var skuList : ArrayList<SkuEntity> = ArrayList()
        var corridorTemp = CorridorWithSkuEntity()
        val noCorridorList : ArrayList<SkuEntity> = ArrayList();
        val withCorridorList : ArrayList<SkuEntity> = ArrayList();
        for(sku in list){
          if (sku.currentCorridor ==  "-" ){
              noCorridorList.add(sku)
          }
            else{
              withCorridorList.add(sku)
          }
        }
        withCorridorList.sortBy { it.currentCorridor }
        tempList.addAll(withCorridorList)
        tempList.addAll(noCorridorList)
        var pasillo = tempList[0].currentCorridor
        for (sku in tempList){
            if (pasillo != sku.currentCorridor){
                corridorTemp.pasillo = pasillo
                corridorTemp.skuList = skuList
                corridorList.add(corridorTemp)
                corridorTemp = CorridorWithSkuEntity()
                pasillo = sku.currentCorridor
                skuList = ArrayList()
            }
            skuList.add(sku)
        }
        corridorTemp.pasillo = pasillo
        corridorTemp.skuList = skuList
        corridorList.add(corridorTemp)
        corridorList.sortWith(compareByDescending<CorridorWithSkuEntity> { it.brokeNumber }
                .thenByDescending { it.productNumber })
        for (entity in corridorList){
            val isBrokeList : ArrayList<SkuEntity> = ArrayList()
            val isNonBrokeList : ArrayList<SkuEntity> = ArrayList()
            for (sku in entity.skuList){
                if (sku.isBroken) isBrokeList.add(sku)
                else isNonBrokeList.add(sku)
            }
            isBrokeList.sortWith(compareBy { it.name })
            isNonBrokeList.sortWith(compareBy { it.name })
            entity.skuList = ArrayList()
            entity.skuList.addAll(isBrokeList)
            entity.skuList.addAll(isNonBrokeList)
        }
        tempList = ArrayList()
        for(lpn in corridorList){
            tempList.addAll(lpn.skuList)
        }
        return tempList
    }

    fun getBrokenNumber(list : ArrayList<SkuEntity>):Int{
        var number = 0
        for(sku in list){
            if (sku.isBroken)number++
        }
        return number
    }

    fun getProductNumber(list : ArrayList<SkuEntity>):Int{
        var number = 0
        for(sku in list){
            number += sku.replaceAmount
        }
        return number
    }
}