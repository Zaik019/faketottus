package com.arquea.tottusnsg.fmp.data.entities.enums

enum class TimStateEnum(val id: Int) {
    DEPARTURE(1) {
        override fun getTimState(): String = "Embarcado"
    },
    IN_PROCESS(2) {
        override fun getTimState(): String = "En proceso"
    },
    PENDING(3) {
        override fun getTimState(): String = "Pendiente"
    },
    DONE(4) {
        override fun getTimState(): String = "Atendido"
    };

    abstract fun getTimState(): String
}