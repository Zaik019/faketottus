package com.arquea.tottusnsg.fmp.core

interface BaseItemCommunicator<T> {
    fun onItemClick(item: T)
}