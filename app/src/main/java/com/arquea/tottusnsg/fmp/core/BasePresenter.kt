package com.arquea.tottusnsg.fmp.core

interface BasePresenter {
    fun start()
}