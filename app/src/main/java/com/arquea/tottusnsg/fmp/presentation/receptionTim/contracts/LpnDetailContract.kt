package com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts

import com.arquea.tottusnsg.fmp.core.BasePresenter
import com.arquea.tottusnsg.fmp.core.BaseView
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.SkuEntity

interface LpnDetailContract {
    interface View : BaseView<Presenter> {
        fun showLpn(lpn: LpnEntity)
        fun showLpnSku(skuList: List<SkuEntity>)
    }

    interface Presenter: BasePresenter {
        fun setLpn(lpn: LpnEntity)
        fun getSku(skuId: String)
    }
}