package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class OcEntity(
        var ocId : String,
        var ocKey : String,
        var ocCreationDate : String,
        var stateId : String,
        var name : String,
        var ruc : String,
        var department : String,
        var buyer : String,
        var ocLimitDate : String,
        var lpnList: List<LpnEntity>,
        var isClassified: Boolean = false
): Serializable{
    companion object {
        fun mockOcList():List<OcEntity>{
            val list : ArrayList<OcEntity> = ArrayList()
            list.add(OcEntity("227506761","227506761","20/11/2018 10:22","0",
                    "Tottus S.A.C.","20511326946-4","Frutas y Verduras",
                    "Baltazar Zapata Luis", "20/11/2018 10:22",
                    LpnEntity.mockOcNonClassifiedLpnList(), false))
            list.add(OcEntity("227506762","227506762","23/10/2018 11:22","1",
                    "Tottus S.A.C.","20511326946-4","Frutas y Verduras",
                    "Baltazar Zapata Luis", "20/11/2018 10:22",
                    LpnEntity.mockOcNonClassifiedLpnList(), false))
            return list
        }
    }
}