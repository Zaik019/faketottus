package com.arquea.tottusnsg.fmp.presentation.home

import android.os.Bundle
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import pe.com.falabella.faketottus.R

class HomeActivity :BaseActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_toolbar)

        setupToolbar("Home")

        val fragment = supportFragmentManager.findFragmentById(R.id.body) as HomeFragment? ?:
        HomeFragment.newInstance().also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}