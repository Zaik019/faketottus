package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.LpnResponse

interface LpnFakeDataSource {
    fun getLpnById(request: LpnRequest, callback: BaseRequestCallback.ApiResponse<LpnResponse>)
}