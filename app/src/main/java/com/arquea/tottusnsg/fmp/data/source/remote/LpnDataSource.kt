package com.arquea.tottusnsg.fmp.data.source.remote

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.LpnResponse

interface LpnDataSource {
    fun getLpnById(request: LpnRequest, callback: BaseRequestCallback.ApiResponse<LpnResponse>)

    //fun setLpnPendingState(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>)
}