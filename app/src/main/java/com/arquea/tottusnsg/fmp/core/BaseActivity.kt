package com.arquea.tottusnsg.fmp.core

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.arquea.tottusnsg.nsg.activities.LoginActivity
import com.arquea.tottusnsg.nsg.dataproviders.SesionProvider
import com.arquea.tottusnsg.nsg.service.infrastructure.ApiClient
import com.arquea.tottusnsg.nsg.service.infrastructure.ClientException
import com.arquea.tottusnsg.nsg.service.infrastructure.ServerException
import com.microsoft.aad.adal.AuthenticationCallback
import com.microsoft.aad.adal.AuthenticationContext
import com.microsoft.aad.adal.AuthenticationResult
import pe.com.falabella.faketottus.BuildConfig
import pe.com.falabella.faketottus.R

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    protected fun setupToolbar(withTitle:String){
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = withTitle
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    protected fun nextActivity(context: Activity, bundle: Bundle?, activity: Class<*>, destroy: Boolean) {
        val intent = Intent(context, activity)
        if (bundle != null)
            intent.putExtras(bundle)

        startActivity(intent)

        if (destroy)
            context.finish()
    }

    protected fun newActivityClearPreview(context: Activity, bundle: Bundle?, activity: Class<*>) {
        val intent = Intent(context, activity)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        if (bundle != null)
            intent.putExtras(bundle)

        startActivity(intent)
        context.finish()
    }

    protected fun nextActivityNewTask(context: Activity, bundle: Bundle?, activity: Class<*>, destroy: Boolean) {
        val intent = Intent(context, activity)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (bundle != null)
            intent.putExtras(bundle)

        startActivity(intent)
        if (destroy) context.finish()
    }

    fun showOkMessage(message: String) {
        val container = findViewById<CoordinatorLayout>(R.id.coordinatorLayout)
        this.showMessageSnack(container, message, R.color.colorGreen1)
    }

    fun showErrorMessage(message: String) {
        val container = findViewById<CoordinatorLayout>(R.id.coordinatorLayout)
        this.showMessageSnack(container, message, R.color.error_red)
    }

    private fun showMessageSnack(container: View?, message: String, colorResource: Int) {
        if (container != null) {
            val snackBar = Snackbar.make(container, message, Snackbar.LENGTH_LONG)
            snackBar.setActionTextColor(Color.WHITE)
            val sbView = snackBar.view
            sbView.setBackgroundColor(ContextCompat.getColor(this, colorResource))

            val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
            textView.setTextColor(Color.WHITE)

            snackBar.show()
        } else {
            val toast = Toast.makeText(applicationContext,
                    message, Toast.LENGTH_LONG)

            toast.show()
        }
    }

    //Base Activity Arquea
    private var currentAccessToken: String? = null
    private var currentIdToken: String? = null

    private fun regresarALogin() {
        AlertDialog.Builder(this)
                .setTitle("Sesión caducada")
                .setMessage("Su sesión ha caducado, por favor inicie sesión una vez más para continuar")
                .setPositiveButton(android.R.string.ok, null)
                .show()
                .setOnDismissListener {
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
    }

    private fun obtenerApiKey(callback: (String, String) -> Unit) {
        if (SesionProvider.IdUsuario == null) {
            regresarALogin()
            return
        }
        val mContext = AuthenticationContext(this, BuildConfig.ADNATIVE_DIRECTORIO, true)
        mContext.acquireTokenSilentAsync(BuildConfig.ADNATIVE_RECURSO, BuildConfig.ADNATIVE_APP_ID, SesionProvider.IdUsuario, object : AuthenticationCallback<AuthenticationResult> {
            override fun onError(exc: Exception?) {
                Log.e("TOTTUSNSG", "Error al conectar al servicio", exc)
                regresarALogin()
            }

            override fun onSuccess(result: AuthenticationResult?) {

                currentAccessToken = result?.accessToken
                currentIdToken = result?.idToken
                if (currentIdToken != null) {
                    currentIdToken = currentIdToken!!.split(".")[1]
                }
                if (currentAccessToken == null) {
                    regresarALogin()
                    return
                }
                callback(currentAccessToken!!, currentIdToken!!)
            }
        })
    }

    fun runApiCall(fn: (host: String) -> Unit, errorFn: ((error:Exception?) -> Unit)? = null, reintentar: Boolean = true) {
        obtenerApiKey { accessToken, idToken ->
            try {
                val map = HashMap<String, String>()
                map["X-API-KEY"] = accessToken
                map["ID-TOKEN"] = idToken
                map["Content-Type"] = "application/json"
                map["Accept"] = "application/json"
                ApiClient.defaultHeaders = map
                fn(BuildConfig.BFF_HOST)
            } catch (errorCliente: ClientException) {
                Log.w("TOTTUSNSG", "Error en runApiCall en la petición (bad request o algún 400)", errorCliente)
                if (errorFn != null) {
                    errorFn(errorCliente)
                }
            } catch (errorServer: ServerException) {
                if (reintentar) {
                    runApiCall(fn, errorFn, false) //Intentamos solo una vez más
                } else {
                    Log.e("TOTTUSNSG", "Error de servidor en runApiCall, crítico", errorServer)
                    if (errorFn != null) {
                        errorFn(errorServer)
                    } else {
                        AlertDialog.Builder(this)
                                .setTitle("Error de servidor")
                                .setMessage("No se ha podido consultar al servicio, por favor vuelva a intentar en unos momentos y asegúrece de estar conectado a internet.")
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                    }
                }
            } catch (error: Exception) {
                Log.w("TOTTUSNSG", "Error de aplicación en runApiCall", error)
                if (errorFn != null) {
                    errorFn(error)
                }
            }
        }
    }



}