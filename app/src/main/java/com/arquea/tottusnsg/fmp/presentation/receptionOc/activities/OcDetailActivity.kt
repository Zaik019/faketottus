package com.arquea.tottusnsg.fmp.presentation.receptionOc.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.entities.OcEntity
import com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments.OcDetailFragment
import com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments.OcLpnFragment
import com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments.OcSkuFragment
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import kotlinx.android.synthetic.main.bs_activity_tab_toolbar.*
import pe.com.falabella.faketottus.R

class OcDetailActivity : BaseActivity() {
    lateinit var dataTab : TabLayout.Tab
    lateinit var skuTab : TabLayout.Tab
    lateinit var ocEntity: OcEntity

    private var fragment : Fragment? = null

    companion object {
        const val EXTRA_OC = "OC"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_tab_toolbar)

        ocEntity = intent.extras!!.get(EXTRA_OC) as OcEntity

        setupTabBar()
        setupToolbar("OC ${ocEntity.ocId.takeLast(4)}")

        fragment = OcDetailFragment.newInstance(ocEntity)
        ActivityUtils.replaceFragment(supportFragmentManager,R.id.body, fragment as OcDetailFragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onBackPressed() {
        finish()
    }

    private fun setupTabBar(){
        dataTab = tabs.newTab().setText("Datos")
        tabs.addTab(dataTab)
        skuTab = tabs.newTab()
        if (ocEntity.stateId == "0"){
            skuTab.text = "SKU"
        }
        else{
            skuTab.text = "LPN"
        }
        tabs.addTab(skuTab)

        tabs.addOnTabSelectedListener(
                object : TabLayout.OnTabSelectedListener {
                    override fun onTabSelected(tab: TabLayout.Tab) {
                        when(tab){
                            dataTab -> {
                                fragment = OcDetailFragment.newInstance(ocEntity)
                                ActivityUtils.replaceFragment(supportFragmentManager,R.id.body, fragment as OcDetailFragment)
                            }
                            skuTab -> {
                                if (ocEntity.stateId == "0") {
                                    fragment = OcSkuFragment.newInstance(ocEntity.lpnList[0])
                                    ActivityUtils.replaceFragment(supportFragmentManager,R.id.body, fragment as OcSkuFragment)
                                } else{
                                    fragment = OcLpnFragment.newInstance()
                                    ActivityUtils.replaceFragment(supportFragmentManager,R.id.body, fragment as OcLpnFragment)
                                }
                            }
                        }
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {

                    }

                    override fun onTabReselected(tab: TabLayout.Tab) {

                    }
                }
        )
    }

}