package com.arquea.tottusnsg.fmp.services

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.support.v4.app.NotificationCompat
import android.support.v4.content.FileProvider
import android.util.Base64
import android.webkit.MimeTypeMap
import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.ReportResponse
import com.arquea.tottusnsg.fmp.data.entities.enums.DownloadEnum
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl.ReportPdfFakeRepository
import pe.com.falabella.faketottus.BuildConfig
import pe.com.falabella.faketottus.R
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class FMDownloadService : IntentService("FMDownloadService") {

    private lateinit var notificationBuilder: NotificationCompat.Builder
    private lateinit var notificationManager: NotificationManager
    private lateinit var sessionManager: SessionManager

    private lateinit var title : String

    private var random = Random()
    private var m = random.nextInt(9999 - 1000) + 1000
    private var dirFile = ""
    private val id = "notify_001"
    private var fileName = ""

    private lateinit var reportPdfFakeRepository: ReportPdfFakeRepository

    override fun onHandleIntent(intent: Intent?) {
        sessionManager = SessionManager(applicationContext)
        reportPdfFakeRepository = ReportPdfFakeRepository.getInstance()

        setTitle()

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationBuilder = NotificationCompat.Builder(applicationContext,id)
                .setContentTitle(title)
                .setContentText("Descargando archivo...")
                .setAutoCancel(true)
        notificationBuilder.setSmallIcon(R.drawable.ic_file_download)
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.file_download))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(id,
                    title,
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(m, notificationBuilder.build())

        initDownload()
    }

    private fun setTitle(){
        val originReport = sessionManager.originReport
        title = if (originReport != "") {
            when {
                DownloadEnum.getEnum(originReport) == DownloadEnum.TIM_DOWNLOAD -> "Reporte de TIM"
                DownloadEnum.getEnum(originReport) == DownloadEnum.LPN_DOWNLOAD -> "Reporte de LPN"
                else -> "Descarga"
            }
        } else {
            "Error"
        }
    }

    private fun initDownload(){
        val originReport = sessionManager.originReport
        if (originReport != ""){
            when {
                DownloadEnum.getEnum(originReport) == DownloadEnum.TIM_DOWNLOAD -> {
                    val timReportRequest = sessionManager.timReportRequest
                    if (timReportRequest!=null)
                        generateTimReport(timReportRequest)
                    else {
                        title = "Error"
                        showMessageError("Error al obtener el objeto")
                    }
                }
                DownloadEnum.getEnum(originReport) == DownloadEnum.LPN_DOWNLOAD -> {
                    val lpnReportRequest = sessionManager.lpnReportRequest
                    if (lpnReportRequest!=null)
                        generateLpnReport(lpnReportRequest)
                    else {
                        title = "Error"
                        showMessageError("Error al obtener el objeto")
                    }
                }
                else -> {
                    title = "Indicador"
                    showMessageError("Seleccione un indicador para descargar por favor")
                }
            }
        }
    }

    private fun generateTimReport(timRequest: TimRequest){
        reportPdfFakeRepository.downloadReportTimPdf(timRequest,object : BaseRequestCallback.ApiResponse<ReportResponse>{
            override fun onSuccess(response: ReportResponse) {
                downloadPdf(response,DownloadEnum.TIM_DOWNLOAD)
            }

            override fun onError(errorMessage: String) {
                showMessageError(errorMessage)
            }

            override fun onNetworkError() {
                showMessageError("No se pudo conectar al servidor")
            }

        })
    }

    private fun generateLpnReport(lpnRequest: LpnRequest){
        reportPdfFakeRepository.downloadReportLpnPdf(lpnRequest,object : BaseRequestCallback.ApiResponse<ReportResponse>{
            override fun onSuccess(response: ReportResponse) {
                downloadPdf(response,DownloadEnum.LPN_DOWNLOAD)
            }

            override fun onError(errorMessage: String) {
                showMessageError(errorMessage)
            }

            override fun onNetworkError() {
                showMessageError("No se pudo conectar al servidor")
            }

        })
    }

    private fun downloadPdf(response: ReportResponse,type : DownloadEnum){
        var answer: String

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd_MM_yyyy|HH_mm_ss")
            answer = current.format(formatter)
        } else {
            var date = Date();
            val formatter = SimpleDateFormat("dd_MM_yyyy|HH_mm_ss")
            answer = formatter.format(date)
        }

        val typeString = if (type == DownloadEnum.TIM_DOWNLOAD) "TIM: ${sessionManager.timReportRequest!!.timId}|" else "LPN: ${sessionManager.lpnReportRequest!!.lpnId.split("/")[0]}|"
        val nameFile = typeString.plus(answer)
        fileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath+"/"+"$nameFile.pdf"

        val outputFile = File(fileName)
        val pdfAsBytes = Base64.decode(response.reportEntity!!.encodedBase64,0)
        val os: FileOutputStream
        os = FileOutputStream(outputFile, false)
        os.write(pdfAsBytes)
        onDownloadComplete()
        os.flush()
        os.close()
    }

    private fun onDownloadComplete() {

        val contentText = "Descarga completada\n$fileName"

        var bigTextStyle = NotificationCompat.BigTextStyle()
        bigTextStyle = bigTextStyle.bigText(contentText)

        notificationBuilder.setStyle(bigTextStyle)

        notificationManager.cancel(0)
        notificationBuilder.setProgress(0, 0, false)
        notificationBuilder.setContentTitle(title)

        notificationBuilder.setSmallIcon(R.drawable.ic_cloud_done_white)
        //Drawable d = new BitmapDrawable(getResources(), bmpFinal);
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.pdf_downloaded))

        notificationBuilder.setContentText(contentText)

        //Vibration
        notificationBuilder.setVibrate(longArrayOf(1000, 1000)) //Esto controla el # de repeticiones

        //LED
        notificationBuilder.setLights(Color.GREEN, 300, 100)

        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)

        val file = File(fileName) // set your audio path

        val mime = MimeTypeMap.getSingleton()
        val ext = file.name.substring(file.name.indexOf(".") + 1)
        val type = mime.getMimeTypeFromExtension(ext)
        val openFile = Intent(Intent.ACTION_VIEW)
        //openFile.setDataAndType(Uri.fromFile(file), type);

        //val fileURI = FileProvider.getUriForFile(applicationContext, authority, file)

        val photoURI = FileProvider.getUriForFile(applicationContext,
                BuildConfig.APPLICATION_ID + ".provider",
                file)
        openFile.setDataAndType(photoURI, type)
        openFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        openFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

        val pIntent = PendingIntent.getActivity(this, 0, openFile, PendingIntent.FLAG_CANCEL_CURRENT)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationBuilder.setContentIntent(pIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("notify_001",
                    title,
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(m, notificationBuilder.build())

        //openFolder();

        this.stopSelf()
        //stopService(intent);
    }


    private fun showMessageError(msg: String) {
        showMessage(msg)
        //sessionManager.setDownloadProcess("FINISHED")
        this.stopSelf()
    }

    private fun showMessage(msg: String) {

        var bigTextStyle = NotificationCompat.BigTextStyle()
        bigTextStyle = bigTextStyle.bigText(msg)

        notificationBuilder.setStyle(bigTextStyle)
        notificationManager.cancel(0)

        notificationBuilder.setSmallIcon(R.drawable.ic_warning_white)
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.error))

        notificationBuilder.setProgress(0, 0, false)
        notificationBuilder.setContentTitle(title)
        notificationBuilder.setContentText(msg)

        //Vibration
        notificationBuilder.setVibrate(longArrayOf(1000, 1000)) //Esto controla el # de repeticiones

        //LED
        notificationBuilder.setLights(Color.BLUE, 300, 100)

        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("notify_001",
                    title,
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(m, notificationBuilder.build())
    }


}