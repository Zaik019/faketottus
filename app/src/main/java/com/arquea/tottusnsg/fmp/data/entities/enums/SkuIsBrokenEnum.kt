package com.arquea.tottusnsg.fmp.data.entities.enums

import pe.com.falabella.faketottus.R


enum class SkuIsBrokenEnum(val isBroken: Boolean) {
    IS_BROKEN(true) {
        override fun getStateBackground(): Int = R.drawable.background_broke_red
        override fun getAlertResource(): Int = R.drawable.ic_alert
        override fun getViewBackground(): Int = R.color.red
    },
    IS_NOT_BROKEN(false) {
        override fun getStateBackground(): Int = R.drawable.background_broke_green
        override fun getAlertResource(): Int = 0
        override fun getViewBackground(): Int = R.color.colorAccent
    };

    abstract fun getStateBackground(): Int
    abstract fun getAlertResource(): Int
    abstract fun getViewBackground(): Int

    companion object {
        fun valueOf(status: Boolean): SkuIsBrokenEnum? = SkuIsBrokenEnum.values().find { it.isBroken == status }
    }
}