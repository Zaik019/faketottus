package com.arquea.tottusnsg.fmp.data.dto.requests

data class TimRequest(
        var timId: String,
        var storeId:String
) {
}