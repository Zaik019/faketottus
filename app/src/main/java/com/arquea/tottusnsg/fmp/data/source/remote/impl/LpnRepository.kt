package com.arquea.tottusnsg.fmp.data.source.remote.impl

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.LpnResponse
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.source.remote.LpnDataSource

class LpnRepository : LpnDataSource {

    companion object {
        private var INSTANCE: LpnRepository? = null

        fun getInstance(): LpnRepository {
            if (INSTANCE == null) {
                INSTANCE = LpnRepository()
            }

            return INSTANCE!!
        }
    }

    override fun getLpnById(request: LpnRequest, callback: BaseRequestCallback.ApiResponse<LpnResponse>) {
        val lpnList: List<LpnEntity> = LpnEntity.mockClassifiedLpnList()
        val lpnResponse = LpnResponse()
        for (lpn in lpnList)
            if (lpn.lpnId == request.lpnId) {
                lpnResponse.lpnEntity = lpn
                callback.onSuccess(lpnResponse)
                break
            }

        if (lpnResponse.lpnEntity == null)
            callback.onError("No se encontró el Lpn: " + request.lpnId)
    }
}