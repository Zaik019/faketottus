package com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.data.entities.enums.DownloadEnum
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.presentation.receptionTim.activities.LpnDetailActivity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.adapters.TimDetailAdapter
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.TimDetailContract
import com.arquea.tottusnsg.fmp.services.FMDownloadService
import kotlinx.android.synthetic.main.sp1_rec_fragment_tim_detail.*
import pe.com.falabella.faketottus.R

class TimDetailFragment : BaseFragment(), TimDetailContract.View {

    private lateinit var timEntity: TimEntity

    private lateinit var timDetailAdapter: TimDetailAdapter

    override lateinit var presenter: TimDetailContract.Presenter

    private var scanItem : MenuItem? = null
    private var exportPDFItem : MenuItem? = null
    private lateinit var sessionManager: SessionManager
    private val PERMISSION_REQUEST_CODE = 1

    override var isActive: Boolean = false
        get() = isAdded


    companion object {
        private const val EXTRA_TIM = "TIM"

        fun newInstance(timExtra: TimEntity?): TimDetailFragment {
            val fragment = TimDetailFragment()
            val args = Bundle()

            args.putSerializable(EXTRA_TIM, timExtra)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        sessionManager = SessionManager(context!!)
        if (arguments != null) {
            timEntity = arguments!!.getSerializable(EXTRA_TIM) as TimEntity
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.export_scan_detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        scanItem = menu.findItem(R.id.it_scan_barcode)
        exportPDFItem = menu.findItem(R.id.it_export_pdf)
        menuItemVisibility()
        super.onPrepareOptionsMenu(menu)
    }

    private fun menuItemVisibility(){
        if (scanItem!= null && exportPDFItem!=null) {
            scanItem!!.isVisible = timEntity.isClassified
            exportPDFItem!!.isVisible = timEntity.isClassified
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.it_export_pdf -> {
            val request = TimRequest(timEntity.timId,sessionManager.storeId)
            sessionManager.timReportRequest = request
            sessionManager.originReport = DownloadEnum.TIM_DOWNLOAD.title
            if (checkPermission()) {
                startDownload()
            } else {
                requestPermission()
            }
            true
        }

        R.id.it_scan_barcode -> {
            // Cuando el usuario selecciona opción escanear código de barras
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sp1_rec_fragment_tim_detail, container, false)
    }

    override fun onStart() {
        super.onStart()
        btn_classify.setOnClickListener {
            presenter.checkTimArriveDate(timEntity.timId)
        }

        //timDetailAdapter = TimDetailAdapter(ArrayList()) { lpnItem: LpnEntity -> presenter.getLpn(lpnItem.lpnId)}
        timDetailAdapter = TimDetailAdapter(ArrayList()) { lpnItem: LpnEntity -> presenter.getLpn(lpnItem) }

        setupClassificationButton()

        rv_lpn.layoutManager = LinearLayoutManager(activity)
        rv_lpn.adapter = timDetailAdapter
    }

    override fun onResume() {
        super.onResume()
        presenter.setTim(timEntity)
    }

    override fun setError(msg: String) {
        if(isActive) Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    override fun showTim(tim: TimEntity) {
        timDetailAdapter.isClassified = tim.isClassified
        timEntity = tim
        setupClassificationButton()
        menuItemVisibility()
        showTimLpn(tim.lpnList)
    }

    private fun setupClassificationButton(){
        if (timEntity.isClassified || timEntity.lpnList.isEmpty()) {
            btn_classify.visibility = View.GONE
        } else {
            btn_classify.visibility = View.VISIBLE
        }
    }

    override fun showTimLpn(lpnList: List<LpnEntity>) {
        if (rv_lpn != null)
            //rv_lpn.adapter = TimDetailAdapter(lpnList as ArrayList<LpnEntity>) { lpnItem: LpnEntity -> presenter.getLpn(lpnItem) }
            timDetailAdapter.updateList(lpnList as ArrayList<LpnEntity>)
    }

    override fun showLpnDetail(lpn: LpnEntity) {
        lpn.idTim = timEntity.timId
        val bundle = Bundle()
        bundle.putSerializable(LpnDetailActivity.EXTRA_LPN, lpn)
        next(activity as Activity, bundle, LpnDetailActivity::class.java, false)
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(activity as Activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
    }

    private fun startDownload() {
        val intent = Intent(context, FMDownloadService::class.java)
        activity!!.startService(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startDownload()
            } else {
                Toast.makeText(context, "Permiso denegado!", Toast.LENGTH_LONG).show()
            }
        }
    }
}