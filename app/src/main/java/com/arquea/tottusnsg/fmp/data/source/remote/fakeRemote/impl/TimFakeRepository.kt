package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.TimResponse
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.data.source.remote.ServiceFactory
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.TimFakeDataSource
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request.TimFakeRequest
import com.arquea.tottusnsg.fmp.utils.OrderList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TimFakeRepository : TimFakeDataSource {
    companion object {
        private var INSTANCE: TimFakeRepository? = null

        fun getInstance(): TimFakeRepository {
            if (INSTANCE == null) {
                INSTANCE = TimFakeRepository()
            }

            return INSTANCE!!
        }
    }

    override fun getTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>) {
        val request : TimFakeRequest = ServiceFactory.createService(TimFakeRequest::class.java)
        val idStore : Int = try{timRequest.storeId.toInt()}catch (e : Exception){0}
        try {
            val idTim = timRequest.timId.toLong()
            val call: Call<ArrayList<Map<String, Any>>> = request.searchTimById(2, idStore, idTim)
            call.enqueue(object : Callback<ArrayList<Map<String, Any>>> {
                override fun onFailure(call: Call<ArrayList<Map<String, Any>>>, t: Throwable) {
                    callback.onNetworkError()
                }

                override fun onResponse(call: Call<ArrayList<Map<String, Any>>>, response: Response<ArrayList<Map<String, Any>>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val timResponse = mapGenericResponseToTimResponse(response.body()!!)
                            callback.onSuccess(timResponse)
                        } else {
                            callback.onError("Hubo un error al traer los datos")
                        }
                    } else {
                        callback.onError("Hubo un error al traer los datos")
                    }
                }

            })
        }catch (e : Exception){
            callback.onError("Hubo un error al enviar los datos")
        }
    }

    private fun mapGenericResponseToTimResponse(response:ArrayList<Map<String,Any>>) : TimResponse{
        val tim = TimResponse()
        val timEntity = TimEntity("","","","","","","0","","",ArrayList(),false)
        var lpnList : ArrayList<LpnEntity> = ArrayList()
        for (map in response){
            timEntity.timKey = map["TIMKEY"].toString()
            timEntity.timId = map["TIMID"].toString()
            timEntity.stateId = map["TIMESTADO"].toString()
            val lpnClassification = try {map["LPNCLASIFICACION"].toString().toInt()}catch (e:Exception){0}
            timEntity.isClassified = lpnClassification != 0
            val percentagePriority = try {map["LPNPORCENTAJE"].toString().toDouble()}catch (e:Exception){0.0}
            lpnList.add(LpnEntity(map["LPNKEY"].toString(),map["LPNID"].toString(),map["LPNID"].toString(),
                    lpnClassification,percentagePriority,map["LPNESQUEBRADO"].toString() == "1",
                    map["LPNESALTOVALOR"].toString() == "1",map["LPNESTADO"].toString() == "1",-1,"",ArrayList()))
        }
        lpnList = OrderList.orderLpn(lpnList)
        timEntity.lpnList = lpnList
        tim.timEntity = timEntity
        return tim
    }

    override fun getClassifiedTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>) {
        val request : TimFakeRequest = ServiceFactory.createService(TimFakeRequest::class.java)
        val idStore = timRequest.storeId.toInt()
        try {
            val idTim = timRequest.timId.toLong()
            val call : Call<ArrayList<Map<String, Any>>> =  request.confirmTim(2,idStore,idTim)
            call.enqueue(object : Callback<ArrayList<Map<String, Any>>>{
                override fun onFailure(call: Call<ArrayList<Map<String, Any>>>, t: Throwable) {
                    callback.onNetworkError()
                }

                override fun onResponse(call: Call<ArrayList<Map<String, Any>>>, response: Response<ArrayList<Map<String,Any>>>) {
                    if (response.isSuccessful){
                        if (response.body()!=null){
                            callback.onSuccess(mapGenericResponseToTimResponse(response.body()!!))
                        }
                        else{
                            callback.onError("Hubo un error al traer los datos")
                        }
                    }
                    else{
                        callback.onError("Hubo un error al traer los datos")
                    }
                }

            })
        }catch (e : Exception){
            callback.onError("Hubo un error al enviar los datos")
        }
    }
}
