package com.arquea.tottusnsg.fmp.data.entities.enums

import pe.com.falabella.faketottus.R


enum class LpnEnum(val id: Int) {
    NON_CLASSIFIED(0) {
        override fun getLpnStateBackground(): Int = 0

        override fun getLpnState(): String = "No clasificado"
    },
    TO_REPOSITION(2) {
        override fun getLpnStateBackground(): Int = R.drawable.lpn_status_replace

        override fun getLpnState(): String = "Reponer"
    },
    TO_DESCONSOLIDATE(3) {
        override fun getLpnStateBackground(): Int = R.drawable.lpn_state_separate

        override fun getLpnState(): String = "Desconsolidar"
    },
    TO_WAREHOUSE(1) {
        override fun getLpnStateBackground(): Int = R.drawable.lpn_state_store

        override fun getLpnState(): String = "Almacenar"
    };

    abstract fun getLpnState(): String
    abstract fun getLpnStateBackground(): Int

    companion object {
        fun valueOf(id: Int): LpnEnum? = LpnEnum.values().find { it.id == id }
    }
}