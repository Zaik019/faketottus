package com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.SkuEntity
import com.arquea.tottusnsg.fmp.data.entities.enums.DownloadEnum
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.presentation.receptionTim.adapters.LpnDetailAdapter
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.LpnDetailContract
import com.arquea.tottusnsg.fmp.services.FMDownloadService
import kotlinx.android.synthetic.main.sp1_rec_fragment_lpn_detail.*
import pe.com.falabella.faketottus.R

class LpnDetailFragment : BaseFragment(), LpnDetailContract.View {
    private lateinit var lpnEntity: LpnEntity

    private lateinit var lpnDetailAdapter: LpnDetailAdapter

    override lateinit var presenter: LpnDetailContract.Presenter

    override var isActive: Boolean = false
        get() = isAdded

    private lateinit var sessionManager: SessionManager

    private val PERMISSION_REQUEST_CODE = 1

    companion object {
        private const val EXTRA_LPN = "LPN"

        fun newInstance(lpnExtra: LpnEntity?): LpnDetailFragment {
            val fragment = LpnDetailFragment()
            val args = Bundle()

            args.putSerializable(EXTRA_LPN, lpnExtra)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        sessionManager = SessionManager(context!!)
        if (arguments != null) {
            lpnEntity = arguments!!.getSerializable(EXTRA_LPN) as LpnEntity
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.export_scan_detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.it_export_pdf -> {
            println(lpnEntity.lpnId)
            val request = LpnRequest(lpnEntity.lpnId, lpnEntity.idTim!!,sessionManager.storeId)
            sessionManager.lpnReportRequest = request
            sessionManager.originReport = DownloadEnum.LPN_DOWNLOAD.title
            if (checkPermission()) {
                startDownload()
            } else {
                requestPermission()
            }
            true
        }

        R.id.it_scan_barcode -> {
            // Cuando el usuario selecciona opción escanear código de barras
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.sp1_rec_fragment_lpn_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lpnDetailAdapter = LpnDetailAdapter(ArrayList()) { skuItem: SkuEntity -> presenter.getSku(skuItem.eanCode) }

        rv_sku.layoutManager = LinearLayoutManager(activity)
        rv_sku.adapter = lpnDetailAdapter
    }

    override fun onResume() {
        super.onResume()
        presenter.setLpn(lpnEntity)
    }

    override fun showLpn(lpn: LpnEntity) {
        showLpnSku(lpn.skuList)
    }

    override fun showLpnSku(skuList: List<SkuEntity>) {
        if (rv_sku!=null)
            //rv_sku.adapter = LpnDetailAdapter(skuList as ArrayList<SkuEntity>) { skuItem: SkuEntity -> presenter.getSku(skuItem.eanCode) }
            lpnDetailAdapter.updateItems(skuList as ArrayList<SkuEntity>)
    }

    override fun setError(msg: String) {
        if (isActive) Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(activity as Activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
    }

    private fun startDownload() {
        val intent = Intent(context, FMDownloadService::class.java)
        activity!!.startService(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startDownload()
            } else {
                Toast.makeText(context, "Permiso denegado!", Toast.LENGTH_LONG).show()
            }
        }
    }
}