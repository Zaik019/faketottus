package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request

import com.arquea.tottusnsg.fmp.data.dto.responses.ReportResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ReportFakeRequest {
    @GET("reporte/obtenerLpn/")
    fun getLpnReport(@Query("idPais") idCountry : Int,
                     @Query("idSede") idStore : Int,
                     @Query("idTim") idTim : String,
                     @Query("idLpn") idLpn : String):Call<ReportResponse>
    @GET("reporte/obtenerTim/")
    fun getTimReport(@Query("idPais") idCountry:Int,
                     @Query("idSede") idStore:Int,
                     @Query("idTim") keyTim:String):Call<ReportResponse>
}