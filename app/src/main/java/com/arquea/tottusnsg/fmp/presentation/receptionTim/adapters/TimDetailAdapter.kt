package com.arquea.tottusnsg.fmp.presentation.receptionTim.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.enums.LpnEnum
import kotlinx.android.synthetic.main.bs_item_detail_lpn.view.*
import pe.com.falabella.faketottus.R
import kotlin.math.roundToInt

class TimDetailAdapter(
        var items: ArrayList<LpnEntity>,
        private val clickListener: (LpnEntity) -> Unit
) : RecyclerView.Adapter<TimDetailAdapter.LpnVH>() {
    var isClassified : Boolean = false

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LpnVH {
        return LpnVH(LayoutInflater.from(parent.context).inflate(R.layout.bs_item_detail_lpn, parent, false))
    }

    override fun onBindViewHolder(vh: LpnVH, position: Int) {
        val lpn = items[position]
        vh.bind(lpn, clickListener)
    }

    fun updateList(items : ArrayList<LpnEntity>){
        this.items = items
        notifyDataSetChanged()
    }

    inner class LpnVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(lpn: LpnEntity, clickListener: (LpnEntity) -> Unit) {
            itemView.tv_id.text = lpn.lpnId.split("/")[0]
            if (lpn.classification != 0) {
                itemView.tv_percent.text = lpn.priorization.roundToInt().toString() + "%"
                itemView.tv_classification.text = LpnEnum.valueOf(lpn.classification)!!.getLpnState()
                itemView.tv_classification.setBackgroundResource(LpnEnum.valueOf(lpn.classification)!!.getLpnStateBackground())
                if (lpn.isBroken) {
                    itemView.ll_is_broken.setBackgroundResource(R.drawable.background_broke_red)
                    itemView.iv_is_broken.setImageResource(R.drawable.ic_alert)
                } else {
                    itemView.ll_is_broken.setBackgroundResource(R.drawable.background_broke_green)
                    itemView.iv_is_broken.setImageResource(0)
                }
            }

            itemView.setOnClickListener { if (isClassified) clickListener(lpn) }
        }
    }
}