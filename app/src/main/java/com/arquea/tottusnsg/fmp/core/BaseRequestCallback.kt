package com.arquea.tottusnsg.fmp.core

interface BaseRequestCallback {
    interface ApiResponse<T> {
        fun onSuccess(response: T)

        fun onError(errorMessage: String)

        fun onNetworkError()
    }
}