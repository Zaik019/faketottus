package com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts

import com.arquea.tottusnsg.fmp.core.BasePresenter
import com.arquea.tottusnsg.fmp.core.BaseView
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity

interface TimDetailContract {
    interface View : BaseView<Presenter> {
        fun showTim(tim: TimEntity)
        fun showTimLpn(lpnList: List<LpnEntity>)
        fun showLpnDetail(lpn: LpnEntity)
    }

    interface Presenter: BasePresenter {
        fun setTim(tim: TimEntity)
        fun getLpn(lpn: LpnEntity)
        fun checkTimArriveDate(timId: String)
    }
}