package com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters

import android.content.Context
import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.LpnResponse
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl.LpnFakeRepository
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.LpnDetailContract

class LpnDetailPresenter(
        private val context: Context,
        private val view: LpnDetailContract.View
) : LpnDetailContract.Presenter {
    override fun setLpn(lpn: LpnEntity) {
        lpnEntity = lpn
        start()
    }

    private lateinit var lpnEntity: LpnEntity
    private val lpnRepository: LpnFakeRepository
    private val sessionManager : SessionManager

    init {
        view.presenter = this
        lpnRepository = LpnFakeRepository.getInstance()
        sessionManager = SessionManager(context)
    }

    override fun start() {
        if (lpnEntity != null) {
            val lpnRequest = LpnRequest(lpnEntity.lpnId,lpnEntity.idTim!!,sessionManager.storeId)
            lpnRepository.getLpnById(lpnRequest,object :BaseRequestCallback.ApiResponse<LpnResponse>{
                override fun onSuccess(response: LpnResponse) {
                    if (view.isActive) view.showLpn(response.lpnEntity!!)
                }

                override fun onError(errorMessage: String) {
                    view.setError(errorMessage)
                }

                override fun onNetworkError() {
                    view.setError("No se pudo conectar al servidor")
                }

            })
        }
    }

    override fun getSku(lpnId: String) {
        /*
        * val timRequest = TimRequest(timId)

            timRepository.getClassifiedTimById(timRequest, object : BaseRequestCallback.ApiResponse<TimResponse> {
                override fun onSuccess(response: TimResponse) {
                    view.showTim(response.timEntity!!)
                }

                override fun onError(errorMessage: String) {
                    view.setError(errorMessage)
                }

                override fun onNetworkError() {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
        * */
    }
}