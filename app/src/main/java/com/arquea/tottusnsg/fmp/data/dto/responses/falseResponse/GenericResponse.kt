package com.arquea.tottusnsg.fmp.data.dto.responses.falseResponse

import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

data class GenericResponse(val response: ArrayList<Map<String,JvmType.Object>>) {

}