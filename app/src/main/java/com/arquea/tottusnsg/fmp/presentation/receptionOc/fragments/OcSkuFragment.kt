package com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import pe.com.falabella.faketottus.R

class OcSkuFragment : BaseFragment() {

    companion object {
        fun newInstance(lpnEntity: LpnEntity) : OcSkuFragment {
            val fragment = OcSkuFragment()
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_LPN,lpnEntity)
            fragment.arguments = bundle
            return fragment
        }

        private const val EXTRA_LPN = "LPN"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.sp2_rec_fragment_oc_sku_detail,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}