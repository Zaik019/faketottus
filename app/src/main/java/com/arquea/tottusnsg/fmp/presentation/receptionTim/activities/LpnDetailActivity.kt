package com.arquea.tottusnsg.fmp.presentation.receptionTim.activities

import android.os.Bundle
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments.LpnDetailFragment
import com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters.LpnDetailPresenter
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import pe.com.falabella.faketottus.R

class LpnDetailActivity: BaseActivity() {
    companion object {
        const val EXTRA_LPN = "LPN"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_toolbar)

        val lpnEntity = intent.extras!!.get(EXTRA_LPN) as LpnEntity
        setupToolbar("LPN ${lpnEntity.lpnId.split("/")[0].takeLast(4)}")

        val lpnDetailFragment = supportFragmentManager.findFragmentById(R.id.body) as LpnDetailFragment? ?:
        LpnDetailFragment.newInstance(lpnEntity).also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }

        LpnDetailPresenter(applicationContext,lpnDetailFragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}