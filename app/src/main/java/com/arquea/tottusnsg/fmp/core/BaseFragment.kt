package com.arquea.tottusnsg.fmp.core

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*

open class BaseFragment : Fragment() {
    protected fun next(context: Activity, bundle: Bundle?, activity: Class<*>, destroy: Boolean) {
        val intent = Intent(context, activity)

        if (bundle != null)
            intent.putExtras(bundle)

        startActivity(intent)

        if (destroy)
            context.finish()
    }

    protected fun nextAll(context: Activity, bundle: Bundle?, activity: Class<*>, destroyAll: Boolean) {
        val intent = Intent(context, activity)
        if (bundle != null)
            intent.putExtras(bundle)

        if (destroyAll)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        startActivity(intent)
    }

    protected fun newActivityClearPreview(context: Activity, bundle: Bundle?, activity: Class<*>) {
        val intent = Intent(context, activity)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        if (bundle != null)
            intent.putExtras(bundle)

        startActivity(intent)

        context.finish()
    }

    protected fun openKeyboard(element: View) {
        val inputMethodManager = Objects.requireNonNull<Context>(context).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(element, InputMethodManager.SHOW_IMPLICIT)
    }

    protected fun closeKeyboard() {
        var inputManager : InputMethodManager? = null
        if (context!=null) {
            inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        }
        inputManager?.hideSoftInputFromWindow(Objects.requireNonNull<View>(Objects.requireNonNull<FragmentActivity>(activity).currentFocus).windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun showOkMessage(message: String) {
        (activity as BaseActivity).showOkMessage(message)
    }

    fun showErrorMessage(message: String) {
        (activity as BaseActivity).showErrorMessage(message)
    }
}