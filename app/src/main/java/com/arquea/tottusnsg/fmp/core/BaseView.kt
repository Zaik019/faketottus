package com.arquea.tottusnsg.fmp.core

interface BaseView<T> {
    var presenter: T
    var isActive: Boolean
    fun setError(msg: String)
}