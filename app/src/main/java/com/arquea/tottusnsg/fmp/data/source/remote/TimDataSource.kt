package com.arquea.tottusnsg.fmp.data.source.remote

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.TimResponse

interface TimDataSource {
    fun getTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>)

    fun getClassifiedTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>)
}