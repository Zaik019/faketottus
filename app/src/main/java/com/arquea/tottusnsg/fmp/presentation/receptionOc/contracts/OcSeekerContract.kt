package com.arquea.tottusnsg.fmp.presentation.receptionOc.contracts

import com.arquea.tottusnsg.fmp.core.BasePresenter
import com.arquea.tottusnsg.fmp.core.BaseView
import com.arquea.tottusnsg.fmp.data.entities.OcEntity

interface OcSeekerContract {
    interface View : BaseView<Presenter>{
        fun showOc(ocEntity: OcEntity)
    }
    interface Presenter : BasePresenter{
        fun getOc(ocId: String)
    }
}