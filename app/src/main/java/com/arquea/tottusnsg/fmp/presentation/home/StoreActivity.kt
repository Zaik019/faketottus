package com.arquea.tottusnsg.fmp.presentation.home

import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import pe.com.falabella.faketottus.R


class StoreActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_toolbar)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Seleccionar Tienda"

        supportFragmentManager.findFragmentById(R.id.body) as StoreFragment? ?:
        StoreFragment.newInstance().also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }

    }

}