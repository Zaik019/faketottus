package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class TimEntity(
        var timKey: String,
        var timId: String,
        var departureDate: String,
        var averageDate: String,
        var arriveDate: String,
        var receptionDate: String,
        var stateId: String,
        var shipLocationId: String,
        var receptionLocationId: String,
        var lpnList: List<LpnEntity>,
        var isClassified: Boolean = false
) : Serializable {
    companion object {
        fun mockTimList():List<TimEntity>{
            val list : ArrayList<TimEntity> = ArrayList()
            list.add(TimEntity("123456k","123456","23/10/2018 10:22","23/10/2018 12:00",
                    "","","2","69","41",
                    LpnEntity.mockNonClassifiedLpnList(), false))
            list.add(TimEntity("987654k","987654","23/10/2018 11:22","23/10/2018 13:00",
                    "","","1","69","23",
                    LpnEntity.mockNonClassifiedLpnList(), false))
            return list
        }
    }
}