package com.arquea.tottusnsg.fmp.presentation.receptionTim.activities

import android.os.Bundle
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments.TimSeekerFragment
import com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters.TimSeekerPresenter
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import pe.com.falabella.faketottus.R

class TimSeekerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_toolbar)
        setupToolbar(getString(R.string.title_search_tim))

        val timSeekerFragment = supportFragmentManager.findFragmentById(R.id.body) as TimSeekerFragment? ?:
        TimSeekerFragment.newInstance().also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }

        TimSeekerPresenter(applicationContext,timSeekerFragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}