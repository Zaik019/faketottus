package com.arquea.tottusnsg.fmp.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity

object ActivityUtils {
    fun createFragment(supportFragmentManager:FragmentManager,id:Int,fragment: Fragment){
        supportFragmentManager
                .beginTransaction()
                .add(id, fragment)
                .commit()
    }
    fun replaceFragment(supportFragmentManager: FragmentManager,id: Int,fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(id, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    fun nextActivity(fromActivity:Context,toActivity: Class<*>,bundle: Bundle?){
        val intent = Intent(fromActivity,toActivity)
        if (bundle!=null){
            intent.putExtras(bundle)
        }
        fromActivity.startActivity(intent)
    }

    fun nextActivity(fromActivity:AppCompatActivity,toActivity: AppCompatActivity,bundle: Bundle?){
        val intent = Intent(fromActivity,toActivity::class.java)
        if (bundle!=null){
            intent.putExtras(bundle)
        }
        fromActivity.startActivity(intent)
    }
}