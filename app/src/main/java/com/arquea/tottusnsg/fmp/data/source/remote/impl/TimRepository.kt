package com.arquea.tottusnsg.fmp.data.source.remote.impl

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.TimResponse
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.data.source.remote.TimDataSource

class TimRepository : TimDataSource {
    companion object {
        private var INSTANCE: TimRepository? = null

        fun getInstance(): TimRepository {
            if (INSTANCE == null) {
                INSTANCE = TimRepository()
            }

            return INSTANCE!!
        }
    }

    override fun getTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>) {
        val timList: List<TimEntity> = TimEntity.mockTimList()
        val timResponse = TimResponse()
        for (tim in timList)
            if (tim.timId == timRequest.timId) {
                timResponse.timEntity = tim
                callback.onSuccess(timResponse)
                break
            }

        if (timResponse.timEntity == null)
            callback.onError("No se encontró la Tim: " + timRequest.timId)
    }

    override fun getClassifiedTimById(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<TimResponse>) {
        val timList: List<TimEntity> = TimEntity.mockTimList()
        val timResponse = TimResponse()
        for (tim in timList)
            if (tim.timId == timRequest.timId) {
                tim.lpnList = LpnEntity.mockClassifiedLpnList()
                tim.isClassified = true
                timResponse.timEntity = tim
                callback.onSuccess(timResponse)
                break
            }

        if (timResponse.timEntity == null)
            callback.onError("No se encontró la Tim: " + timRequest.timId)
    }
}