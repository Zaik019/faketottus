package com.arquea.tottusnsg.fmp.data.dto.requests

data class LpnRequest(
        var lpnId: String,
        var timId: String,
        var storeId: String
) {
}