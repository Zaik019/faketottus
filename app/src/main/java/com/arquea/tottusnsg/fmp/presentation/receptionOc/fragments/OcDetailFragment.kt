package com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.entities.OcEntity
import com.arquea.tottusnsg.fmp.data.entities.enums.TimStateEnum
import kotlinx.android.synthetic.main.sp2_rec_fragment_oc_detail.*
import pe.com.falabella.faketottus.R

class OcDetailFragment : BaseFragment() {

    lateinit var ocEntity: OcEntity

    companion object {
        private const val EXTRA_OC = "OC"

        fun newInstance(ocEntity: OcEntity) : OcDetailFragment{
            val fragment = OcDetailFragment()
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_OC,ocEntity)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            ocEntity = arguments!!.getSerializable(EXTRA_OC) as OcEntity
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.sp2_rec_fragment_oc_detail,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup(){
        tv_name_oc.text = ocEntity.name
        tv_ruc_oc.text = ocEntity.ruc
        tv_department_oc.text = ocEntity.department
        tv_buyer_oc.text = ocEntity.buyer
        tv_creation_date_oc.text = ocEntity.ocCreationDate
        tv_reception_date_oc.text = ocEntity.ocLimitDate
        tv_status_oc.text = TimStateEnum.valueOf(ocEntity.stateId).getTimState()
        btn_classify_oc.setOnClickListener {

        }
    }

}