package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LpnFakeRequest {
    @GET("lpn/")
    fun searchLpnById(@Query("idPais") idCountry : Int,
                      @Query("idSede") idStore : Int,
                      @Query("idTim") idTim : String,
                      @Query("idLpn") idLpn : String):Call<ArrayList<Map<String,Any>>>
}