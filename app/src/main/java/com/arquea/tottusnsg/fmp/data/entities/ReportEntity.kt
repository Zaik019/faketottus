package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class ReportEntity(
        val name: String,
        val encodedBase64: String
) : Serializable{}