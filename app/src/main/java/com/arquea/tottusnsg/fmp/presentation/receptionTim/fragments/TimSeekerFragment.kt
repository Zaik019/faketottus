package com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.activities.TimDetailActivity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.TimSeekerContract
import kotlinx.android.synthetic.main.sp1_rec_fragment_tim_seeker.*
import pe.com.falabella.faketottus.R

class TimSeekerFragment : BaseFragment(), TimSeekerContract.View {

    override lateinit var presenter: TimSeekerContract.Presenter

    override var isActive: Boolean = false
        get() = isAdded

    companion object {
        fun newInstance(): TimSeekerFragment = TimSeekerFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.sp1_rec_fragment_tim_seeker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_tim_seeker.setOnClickListener {
            closeKeyboard()
            if (et_tim_id.text.toString().isEmpty()){
                showErrorMessage("El campo no puede ser vacío")
            }
            else {
                presenter.getTim(et_tim_id.text.toString())
            }
        }
    }

    override fun showTim(tim: TimEntity) {
        if (!tim.stateId.equals("n",true)) {
            if (!tim.lpnList.isEmpty()) {
                val bundle = Bundle()
                bundle.putSerializable(TimDetailActivity.EXTRA_TIM, tim)
                next(activity as Activity, bundle, TimDetailActivity::class.java, false)
            } else {
                showErrorMessage("Número de TIM inválido: " + et_tim_id.text.toString())
            }
        }else{
            showErrorMessage("TIM en proceso de embarque")
        }
    }

    override fun setError(msg: String) {
        if (isActive) showErrorMessage(msg)
    }
}