package com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.core.BaseFragment
import pe.com.falabella.faketottus.R

class OcLpnFragment : BaseFragment(){

    companion object {
        fun newInstance():OcLpnFragment = OcLpnFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.sp2_rec_fragment_oc_lpn_detail,container,false)

}