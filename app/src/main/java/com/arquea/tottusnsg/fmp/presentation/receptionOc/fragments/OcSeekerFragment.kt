package com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.entities.OcEntity
import com.arquea.tottusnsg.fmp.presentation.receptionOc.activities.OcDetailActivity
import com.arquea.tottusnsg.fmp.presentation.receptionOc.contracts.OcSeekerContract
import kotlinx.android.synthetic.main.sp1_rec_fragment_tim_seeker.*
import pe.com.falabella.faketottus.R

class OcSeekerFragment : OcSeekerContract.View,BaseFragment() {
    override fun showOc(ocEntity: OcEntity) {
        val bundle = Bundle()
        bundle.putSerializable(OcDetailActivity.EXTRA_OC, ocEntity)
        next(activity as Activity, bundle, OcDetailActivity::class.java, false)
    }

    override lateinit var presenter: OcSeekerContract.Presenter

    override var isActive: Boolean = false
        get() = isAdded

    override fun setError(msg: String) {
        showErrorMessage(msg)
    }

    companion object {
        fun newInstance() : OcSeekerFragment = OcSeekerFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.sp1_rec_fragment_tim_seeker,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        et_tim_id.hint = getString(R.string.et_reposition_insert_oc_code)
        btn_tim_seeker.setOnClickListener {
            closeKeyboard()
            val id = et_tim_id.text.toString()
            if (id.isEmpty()){
                showErrorMessage("El campo no puede ser vacío")
            }
            else {
                presenter.getOc(id)
            }
        }
    }
}