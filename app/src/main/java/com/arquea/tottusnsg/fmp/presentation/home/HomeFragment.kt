package com.arquea.tottusnsg.fmp.presentation.home

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.presentation.receptionOc.activities.OcSeekerActivity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.activities.TimSeekerActivity
import kotlinx.android.synthetic.main.sp1_home_fragment_menu.*
import pe.com.falabella.faketottus.R

class HomeFragment : BaseFragment() {

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sp1_home_fragment_menu,container,false)
    }

    override fun onStart() {
        super.onStart()
        btn_program.setOnClickListener {
            Log.i("click","click")
        }

        btn_reception_tim.setOnClickListener{
            next(activity as Activity, null,TimSeekerActivity::class.java, false)
        }

        btn_reception_oc.setOnClickListener{
            next(activity as Activity, null, OcSeekerActivity::class.java, false)
        }
        btn_replacement.setOnClickListener{
            Log.i("click","click")
        }
    }

}