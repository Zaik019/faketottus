package com.arquea.tottusnsg.fmp.presentation.receptionTim.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arquea.tottusnsg.fmp.data.entities.SkuEntity
import com.arquea.tottusnsg.fmp.data.entities.enums.SkuIsBrokenEnum
import kotlinx.android.synthetic.main.bs_item_detail_sku.view.*
import pe.com.falabella.faketottus.R

open class LpnDetailAdapter(
        var items: ArrayList<SkuEntity>,
        private val clickListener: (SkuEntity) -> Unit
) : RecyclerView.Adapter<LpnDetailAdapter.SkuVH>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkuVH {
        return SkuVH(LayoutInflater.from(parent.context).inflate(R.layout.bs_item_detail_sku, parent, false))
    }

    override fun onBindViewHolder(vh: SkuVH, position: Int) {
        val sku = items[position]
        vh.bind(sku, clickListener)
    }

    fun updateItems(items:ArrayList<SkuEntity>){
        this.items = items
        notifyDataSetChanged()
    }

    inner class SkuVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(sku: SkuEntity, clickListener: (SkuEntity) -> Unit) {
            val replace = "Cant. Reponer: ${sku.replaceAmount}"
            val store = "Cant. Bodega: ${sku.storeAmount}"
            val corridor = "Pasillo: ${sku.currentCorridor}"
            val oh = "Oh: ${sku.onHand}"

            itemView.rl_is_broken.setBackgroundResource(SkuIsBrokenEnum.valueOf(sku.isBroken)!!.getStateBackground())
            itemView.tv_ean.text = sku.eanCode
            itemView.tv_corridor.text = corridor
            itemView.tv_oh.text = oh
            itemView.iv_sku_is_broken.setImageResource(SkuIsBrokenEnum.valueOf(sku.isBroken)!!.getAlertResource())
            itemView.tv_name.text = sku.name
            itemView.vw_line.setBackgroundResource(SkuIsBrokenEnum.valueOf(sku.isBroken)!!.getViewBackground())
            itemView.tv_replace.text = replace
            itemView.tv_store.text = store

            itemView.setOnClickListener { clickListener(sku) }
        }
    }
}

