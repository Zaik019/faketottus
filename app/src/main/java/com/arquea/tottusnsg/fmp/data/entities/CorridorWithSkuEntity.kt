package com.arquea.tottusnsg.fmp.data.entities

import com.arquea.tottusnsg.fmp.utils.OrderList

class CorridorWithSkuEntity {
    var pasillo : String = ""
    var skuList : ArrayList<SkuEntity> = ArrayList()
    var brokeNumber : Int = 0
        get() = if (skuList.isNotEmpty()) OrderList.getBrokenNumber(skuList) else 0
    var productNumber : Int = 0
        get() = if (skuList.isNotEmpty()) OrderList.getProductNumber(skuList) else 0
}