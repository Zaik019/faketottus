package com.arquea.tottusnsg.fmp.presentation.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.arquea.tottusnsg.fmp.core.BaseFragment
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import kotlinx.android.synthetic.main.sp1_home_store.*
import pe.com.falabella.faketottus.R

class StoreFragment : AdapterView.OnItemSelectedListener,BaseFragment(){

    private var idSelected : Int = 0
    private lateinit var sessionManager : SessionManager

    companion object {
        fun newInstance() : StoreFragment=StoreFragment()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (position) {
            0 -> {
                idSelected = 0
                sessionManager.storeId = "86"
            }
            1 -> {
                idSelected = 1
                sessionManager.storeId = "25"
            }
            2 -> {
                idSelected = 2
                sessionManager.storeId = "168"
                //sessionManager.storeId = "228"
            }
            3 -> {
                idSelected = 3
                sessionManager.storeId = "24"
            }
            4 -> {
                idSelected = 4
                sessionManager.storeId = "58"
            }
            else -> {
                idSelected = 0
                sessionManager.storeId = "43"
            }
        }
    }

    private val list : Array<String> = arrayOf("Angamos","Las Begonias","Lima Sur","MegaPlaza","Pachacutec")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.sp1_home_store,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sessionManager = SessionManager(context!!)
        val adapter = ArrayAdapter<String>(context!!,
                android.R.layout.simple_spinner_item, list.sortedBy { it })
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_select_store.adapter = adapter
        sp_select_store.onItemSelectedListener = this

        btn_selection_store.setOnClickListener {
            val intent = Intent(activity as Activity,HomeActivity::class.java)
            startActivity(intent)
        }
    }
}