package com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts

import com.arquea.tottusnsg.fmp.core.BasePresenter
import com.arquea.tottusnsg.fmp.core.BaseView
import com.arquea.tottusnsg.fmp.data.entities.TimEntity

interface TimSeekerContract {
    interface View : BaseView<Presenter> {
        fun showTim(tim: TimEntity)
    }

    interface Presenter: BasePresenter {
        fun getTim(timId: String)
    }
}