package com.arquea.tottusnsg.fmp.presentation.receptionTim.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.data.entities.TimEntity
import com.arquea.tottusnsg.fmp.presentation.receptionTim.fragments.TimDetailFragment
import com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters.TimDetailPresenter
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import kotlinx.android.synthetic.main.bs_activity_tab_toolbar.*
import pe.com.falabella.faketottus.R

class TimDetailActivity : BaseActivity() {

    companion object {
        const val EXTRA_TIM = "TIM"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_tab_toolbar)

        setupTabBar()

        val timEntity = intent.extras!!.get(EXTRA_TIM) as TimEntity

        setupToolbar("TIM ${timEntity.timId.takeLast(4)}")

        val timDetailFragment = supportFragmentManager.findFragmentById(R.id.body) as TimDetailFragment? ?:
        TimDetailFragment.newInstance(timEntity).also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }

        TimDetailPresenter(applicationContext,timDetailFragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setupTabBar(){
        val lpnTab = tabs.newTab().setText("LPN")
        tabs.addTab(lpnTab)
        val skuTab = tabs.newTab().setText("SKU de TIM")
        tabs.addTab(skuTab)

        tabs.addOnTabSelectedListener(
                object : TabLayout.OnTabSelectedListener {
                    override fun onTabSelected(tab: TabLayout.Tab) {

                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {

                    }

                    override fun onTabReselected(tab: TabLayout.Tab) {

                    }
                }
        )
    }
}