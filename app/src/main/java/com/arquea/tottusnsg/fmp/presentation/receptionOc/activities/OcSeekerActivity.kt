package com.arquea.tottusnsg.fmp.presentation.receptionOc.activities

import android.os.Bundle
import com.arquea.tottusnsg.fmp.core.BaseActivity
import com.arquea.tottusnsg.fmp.presentation.receptionOc.fragments.OcSeekerFragment
import com.arquea.tottusnsg.fmp.presentation.receptionOc.presenters.OcSeekerPresenter
import com.arquea.tottusnsg.fmp.utils.ActivityUtils
import pe.com.falabella.faketottus.R

class OcSeekerActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bs_activity_toolbar)
        setupToolbar(getString(R.string.title_search_oc))

        val ocSeekerFragment = supportFragmentManager.findFragmentById(R.id.body) as OcSeekerFragment? ?:
        OcSeekerFragment.newInstance().also {
            ActivityUtils.createFragment(supportFragmentManager, R.id.body, it)
        }

        OcSeekerPresenter(applicationContext,ocSeekerFragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}