package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.ReportResponse

interface ReportPdfDataSource {
    fun downloadReportTimPdf(timRequest: TimRequest,callback: BaseRequestCallback.ApiResponse<ReportResponse>)
    fun downloadReportLpnPdf(lpnRequest: LpnRequest,callback: BaseRequestCallback.ApiResponse<ReportResponse>)
}