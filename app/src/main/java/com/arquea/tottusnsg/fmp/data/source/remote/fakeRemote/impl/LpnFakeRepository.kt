package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.LpnResponse
import com.arquea.tottusnsg.fmp.data.entities.CorridorEntity
import com.arquea.tottusnsg.fmp.data.entities.LpnEntity
import com.arquea.tottusnsg.fmp.data.entities.SkuEntity
import com.arquea.tottusnsg.fmp.data.source.remote.ServiceFactory
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.LpnFakeDataSource
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request.LpnFakeRequest
import com.arquea.tottusnsg.fmp.utils.OrderList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LpnFakeRepository : LpnFakeDataSource{

    companion object {
        private var INSTANCE: LpnFakeRepository? = null

        fun getInstance(): LpnFakeRepository {
            if (INSTANCE == null) {
                INSTANCE = LpnFakeRepository()
            }
            return INSTANCE!!
        }
    }

    override fun getLpnById(request: LpnRequest, callback: BaseRequestCallback.ApiResponse<LpnResponse>) {
        val lpnRequest : LpnFakeRequest = ServiceFactory.createService(LpnFakeRequest::class.java)
        val idStore : Int = request.storeId.toInt()
        println(request.lpnId)
        val call : Call<ArrayList<Map<String,Any>>> = lpnRequest.searchLpnById(2,idStore,request.timId,request.lpnId)
        call.enqueue(object : Callback<ArrayList<Map<String,Any>>>{
            override fun onFailure(call: Call<ArrayList<Map<String, Any>>>, t: Throwable) {
                callback.onNetworkError()
            }

            override fun onResponse(call: Call<ArrayList<Map<String, Any>>>, response: Response<ArrayList<Map<String, Any>>>) {
                if (response.isSuccessful){
                    if (response.body()!=null){
                        val lpnResponse = mapGenericResponseToLpnResponse(response.body()!!)
                        callback.onSuccess(lpnResponse)
                    }
                    else{
                        callback.onError("Hubo un error al traer los datos")
                    }
                }
                else{
                    callback.onError("Hubo un error al traer los datos")
                }
            }

        })
    }

    private fun mapGenericResponseToLpnResponse(list : ArrayList<Map<String,Any>>):LpnResponse{
        val lpn = LpnResponse()
        val lpnEntity = LpnEntity("","","",0,0.0,false,false,false,-1,"",ArrayList())
        var skuList : ArrayList<SkuEntity> = ArrayList()
        for (map in list){
            lpnEntity.lpnKey = map["LPNKEY"].toString()
            lpnEntity.lpnId = map["LPNID"].toString().split("/")[0]
            lpnEntity.classification = try {map["LPNCLASIFICACION"].toString().toInt()}catch (e: Exception){0}
            lpnEntity.priorization = try {map["LPNPORCENTAJE"].toString().toDouble()}catch (e:Exception){0.0}
            val sequenceNumber = try{map["SKUNUMEROSECUENCIA"].toString().toInt()}catch (e:Exception){0}
            val sendAmount = try{map["SKUCANTIDADRECIBIDA"].toString().toInt()}catch (e:Exception){0}
            val quantityFloor = try{map["SKUCANTIDADPISO"].toString().toInt()}catch (e:Exception){0}
            val quantityStore = try{map["SKUCANTIDADBODEGA"].toString().toInt()}catch (e:Exception){0}
            val oh = try{map["SKUONHAND"].toString().toInt()}catch (e:Exception){0}
            val corridorList : ArrayList<CorridorEntity> = ArrayList()
            corridorList.add(CorridorEntity(0,map["SKUPASILLO"].toString()))
            skuList.add(SkuEntity(lpnEntity.lpnKey,lpnEntity.lpnId,map["SKUKEY"].toString(),sequenceNumber,map["SKUCLASE"].toString().trim(),
                    map["SKUNOMBRE"].toString(),map["SKUEAN"].toString(),sendAmount,0,quantityFloor,quantityStore,
                    0,oh,map["ESQUEBRADO"].toString() == "1", map["ESALTOVALOR"].toString() == "1",
                    corridorList))
        }
        if (skuList.isNotEmpty()) {
            skuList = OrderList.orderSku(skuList)
        }
        lpnEntity.skuList = skuList
        lpn.lpnEntity = lpnEntity
        return lpn
    }

}