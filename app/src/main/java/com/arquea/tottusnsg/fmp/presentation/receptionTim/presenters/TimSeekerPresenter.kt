package com.arquea.tottusnsg.fmp.presentation.receptionTim.presenters

import android.content.Context
import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.TimResponse
import com.arquea.tottusnsg.fmp.data.source.local.SessionManager
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl.TimFakeRepository
import com.arquea.tottusnsg.fmp.presentation.receptionTim.contracts.TimSeekerContract

class TimSeekerPresenter(
        context: Context,
        private val timSeekerView: TimSeekerContract.View
) : TimSeekerContract.Presenter {

    private val timRepository: TimFakeRepository
    private val sessionManager : SessionManager

    init {
        timSeekerView.presenter = this
        timRepository = TimFakeRepository.getInstance()
        sessionManager = SessionManager(context)
    }

    override fun start() {
    }

    override fun getTim(timId: String) {
        val timRequest = TimRequest(timId,sessionManager.storeId)

        timRepository.getTimById(timRequest, object : BaseRequestCallback.ApiResponse<TimResponse> {
            override fun onSuccess(response: TimResponse) {
                if (timSeekerView.isActive)
                    timSeekerView.showTim(response.timEntity!!)
            }

            override fun onError(errorMessage: String) {
                timSeekerView.setError(errorMessage)
            }

            override fun onNetworkError() {
                timSeekerView.setError("No se pudo conectar al servidor")
            }
        })
    }
}