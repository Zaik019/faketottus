package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.impl

import com.arquea.tottusnsg.fmp.core.BaseRequestCallback
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.arquea.tottusnsg.fmp.data.dto.responses.ReportResponse
import com.arquea.tottusnsg.fmp.data.source.remote.ServiceFactory
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.ReportPdfDataSource
import com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request.ReportFakeRequest
import retrofit2.Call

import retrofit2.Callback
import retrofit2.Response

class ReportPdfFakeRepository : ReportPdfDataSource{

    companion object {
        private var INSTANCE: ReportPdfFakeRepository? = null

        fun getInstance(): ReportPdfFakeRepository {
            if (INSTANCE == null) {
                INSTANCE = ReportPdfFakeRepository()
            }
            return INSTANCE!!
        }
    }

    override fun downloadReportTimPdf(timRequest: TimRequest, callback: BaseRequestCallback.ApiResponse<ReportResponse>) {
        val request : ReportFakeRequest = ServiceFactory.createService(ReportFakeRequest::class.java)
        val idStore : Int = try{timRequest.storeId.toInt()}catch (e : Exception){0}
        val call = request.getTimReport(2,idStore,timRequest.timId)
        call.enqueue(object : Callback<ReportResponse>{
            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                callback.onNetworkError()
            }

            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful){
                    if (response.body()!=null){
                        if (response.body()!!.state==1) {
                            callback.onSuccess(response.body()!!)
                        }
                        else {
                            callback.onError("Hubo un error al traer los datos")
                        }
                    }
                    else{
                        callback.onError("Hubo un error al traer los datos")
                    }
                }
                else{
                    callback.onError("Hubo un error al traer los datos")
                }
            }

        })
    }

    override fun downloadReportLpnPdf(lpnRequest: LpnRequest, callback: BaseRequestCallback.ApiResponse<ReportResponse>) {
        val request : ReportFakeRequest = ServiceFactory.createService(ReportFakeRequest::class.java)
        val idStore : Int = try{lpnRequest.storeId.toInt()}catch (e : Exception){0}
        val call = request.getLpnReport(2,idStore,lpnRequest.timId,lpnRequest.lpnId)
        call.enqueue(object : Callback<ReportResponse>{
            override fun onFailure(call: Call<ReportResponse>, t: Throwable) {
                callback.onNetworkError()
            }

            override fun onResponse(call: Call<ReportResponse>, response: Response<ReportResponse>) {
                if (response.isSuccessful){
                    if (response.body()!=null){
                        if (response.body()!!.state==1) {
                            callback.onSuccess(response.body()!!)
                        }
                        else{
                            callback.onError("Hubo un error al traer los datos")
                        }
                    }
                    else{
                        callback.onError("Hubo un error al traer los datos")
                    }
                }
                else{
                    callback.onError("Hubo un error al traer los datos")
                }
            }

        })
    }

}