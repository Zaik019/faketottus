package com.arquea.tottusnsg.fmp.data.source.local

import android.content.Context
import android.content.SharedPreferences
import com.arquea.tottusnsg.fmp.data.dto.requests.LpnRequest
import com.arquea.tottusnsg.fmp.data.dto.requests.TimRequest
import com.google.gson.Gson
import java.lang.Exception


class SessionManager(private val context: Context) {

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    var storeId : String
        get() = preferences.getString(STORE_ID,"43")!!
        set(value) {
            editor.putString(STORE_ID,value)
            editor.commit()
        }

    var originReport : String
        get() = preferences.getString(ORIGIN_REPORT,"")!!
        set(value){
            editor.putString(ORIGIN_REPORT,value)
            editor.commit()
        }

    var timReportRequest : TimRequest?
        get() {
            val json = preferences.getString(REPORT_TIM,"")!!
            return try{Gson().fromJson(json,TimRequest::class.java)}catch (e:Exception){null}
        }
        set(value) {
            val json = Gson().toJson(value)
            editor.putString(REPORT_TIM,json)
            editor.commit()
        }

    var lpnReportRequest : LpnRequest?
        get() {
            val json = preferences.getString(REPORT_LPN,"")!!
            return try{Gson().fromJson(json,LpnRequest::class.java)}catch (e:Exception){null}
        }
        set(value) {
            val json = Gson().toJson(value)
            editor.putString(REPORT_LPN,json)
            editor.commit()
        }

    init {
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE)
        editor = preferences.edit()
    }

    companion object {


        const val PREFERENCE_NAME = "Flujo Mercadería"
        const val PRIVATE_MODE = 0

        /**
         * DATA SESSION
         */
        const val STORE_ID = "store_id"
        const val ORIGIN_REPORT = "origin_report"
        const val REPORT_TIM = "report_tim"
        const val REPORT_LPN = "report_lpn"
    }

}
