package com.arquea.tottusnsg.fmp.utils

import android.os.Parcelable
import android.os.Parcel



class Download() : Parcelable {
    private var progress: Int = 0
    private var currentFileSize: Int = 0
    private var totalFileSize: Int = 0

    fun getProgress(): Int {
        return progress
    }

    fun setProgress(progress: Int) {
        this.progress = progress
    }

    fun getCurrentFileSize(): Int {
        return currentFileSize
    }

    fun setCurrentFileSize(currentFileSize: Int) {
        this.currentFileSize = currentFileSize
    }

    fun getTotalFileSize(): Int {
        return totalFileSize
    }

    fun setTotalFileSize(totalFileSize: Int) {
        this.totalFileSize = totalFileSize
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {

        dest.writeInt(progress)
        dest.writeInt(currentFileSize)
        dest.writeInt(totalFileSize)
    }

    constructor(parcel: Parcel) : this() {
        progress = parcel.readInt()
        currentFileSize = parcel.readInt()
        totalFileSize = parcel.readInt()
    }

    companion object CREATOR : Parcelable.Creator<Download> {
        override fun createFromParcel(parcel: Parcel): Download {
            return Download(parcel)
        }

        override fun newArray(size: Int): Array<Download?> {
            return arrayOfNulls(size)
        }
    }

}