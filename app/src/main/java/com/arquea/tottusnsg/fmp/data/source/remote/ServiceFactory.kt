package com.arquea.tottusnsg.fmp.data.source.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pe.com.falabella.faketottus.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceFactory    {
    val API_BASE_URL = BuildConfig.BASE_URL

    // set your desired log level

    private val httpClient = OkHttpClient()
    private val builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

    fun <S> createService(serviceClass: Class<S>): S {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(logging).build()

        val retrofit = builder.client(httpClient).client(client).build()
        return retrofit.create(serviceClass)
    }
}