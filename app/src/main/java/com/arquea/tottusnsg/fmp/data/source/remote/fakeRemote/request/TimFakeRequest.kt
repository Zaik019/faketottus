package com.arquea.tottusnsg.fmp.data.source.remote.fakeRemote.request


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TimFakeRequest {
    @GET("tim/")
    fun searchTimById(@Query("idPais") idCountry:Int,
                      @Query("idSede") idStore:Int,
                      @Query("idTim") keyTim:Long) : Call<ArrayList<Map<String,Any>>>

    @GET("tim/confirmarTim")
    fun confirmTim(@Query("idPais") idCountry:Int,
                   @Query("idSede") idStore:Int,
                   @Query("idTim") keyTim:Long) : Call<ArrayList<Map<String,Any>>>

}