package com.arquea.tottusnsg.fmp.data.entities.enums

enum class DownloadEnum (val title : String) {
    LPN_DOWNLOAD("lpn_download"),
    TIM_DOWNLOAD("tim_download");

    companion object {
        fun getEnum(title: String): DownloadEnum? = DownloadEnum.values().find { it.title == title }
    }

}