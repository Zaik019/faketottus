package com.arquea.tottusnsg.fmp.data.entities

import java.io.Serializable

data class CorridorEntity(
        var corridorId: Int = 0,
        var description: String = ""
) : Serializable {
    companion object {
        fun corridorListMock():List<CorridorEntity>{
            val list : ArrayList<CorridorEntity> = ArrayList()
            list.add(CorridorEntity(3,"J3"))
            list.add(CorridorEntity(13,"Ex3"))
            list.add(CorridorEntity(20,"Ch20"))
            return list
        }
    }
}