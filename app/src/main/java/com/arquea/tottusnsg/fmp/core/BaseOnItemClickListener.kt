package com.arquea.tottusnsg.fmp.core

import android.content.Context

interface BaseAdapterListener {
    fun onItemClick(position: Int, context: Context)
}
